from PyQt4.QtGui import *
from PyQt4.QtCore import *
import MySQLdb


class LogIn(QDialog):

	def __init__(self,parent,sender):
		super(LogIn,self).__init__()
		self.parent = parent
		self.sender = sender
		self.initUI()

	def initUI(self):
		self.setWindowTitle("LogIn")
		self.grid = QGridLayout()
		self.username = QLabel("User Name")
		self.iusername = QLineEdit(self)
		self.password = QLabel("Password")
		self.ipassword = QLineEdit(self)
		self.ipassword.setEchoMode(QLineEdit.Password)

		self.grid.addWidget(self.username,0,0)
		self.grid.addWidget(self.iusername,0,1)
		self.grid.addWidget(self.password,1,0)
		self.grid.addWidget(self.ipassword,1,1)

		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Enter")

		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addWidget(self.reset)

		self.main_layout = QVBoxLayout()
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.setLayout(self.main_layout)
		self.connect(self.reset,SIGNAL("clicked()"),self.clearAll)
		self.connect(self.ok,SIGNAL("clicked()"),self.getAll)


	def getAll(self):
		userName = self.iusername.text()
		password = self.ipassword.text()
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		try:
			if(self.sender=="admin"):
				cursor.execute("select PID from Manager where PID='%d' and Password='%s'"%(int(userName),password))
			elif(self.sender=="reception"):
				cursor.execute("select PID from Employee where PID='%d' and Password='%s'"%(int(userName),password))
			else:
				cursor.execute("select PID from Doctor where PID='%d' and Password='%s'"%(int(userName),password))
			rows = cursor.fetchall()
			db.close()
			if(len(rows)==0):
				message = QMessageBox(QMessageBox.Warning,"Login Message","Invalid Username or Password",buttons = QMessageBox.Close)
				message.exec_()
			else:
				self.close()
				self.parent.updateUI(self.sender,rows[0][0])
		except:
			message = QMessageBox(QMessageBox.Warning,"Login Message","Invalid Username or Password",buttons = QMessageBox.Close)
			message.exec_()


	def clearAll(self):
		self.iusername.setText("")
		self.ipassword.setText("")


# app = QApplication(sys.argv)
# log = LogIn()
# log.exec_()
