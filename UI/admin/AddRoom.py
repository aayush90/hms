from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb



class AddRoom(QFrame):
	def __init__(self):
		super(AddRoom,self).__init__()
		self.initUI()

	def initUI(self):

		self.roomno = QLabel("Room No")
		self.iroomno = QLineEdit()
		self.iroomno.setPlaceholderText("Enter New RoomNo")
		self.building = QLabel("Building")
		self.ibuilding = QLineEdit()
		self.ibuilding.setPlaceholderText("Enter Building Name")
		self.prps = QLabel("Purpose")
		self.iprps = QComboBox()
		self.iprps.addItem("select")
		self.iprps.addItem("admit")
		self.iprps.addItem("operation")
		self.iprps.addItem("test")
		self.iprps.addItem("checkup")
		self.iprps.addItem("other")
		self.dept = QLabel("Department")
		self.idept = QComboBox()
		self.setDept()

		self.roomno.setFixedWidth(150)
		self.iroomno.setFixedWidth(300)
		self.ibuilding.setFixedWidth(300)
		self.iprps.setFixedWidth(300)
		self.idept.setFixedWidth(300)

		self.roomno.setStyleSheet("color: black; background-color:gray")
		self.building.setStyleSheet("color: black; background-color:gray")
		self.prps.setStyleSheet("color: black; background-color:gray")
		self.dept.setStyleSheet("color: black; background-color:gray")

		self.iroomno.setStyleSheet("color: black; background-color:white")
		self.ibuilding.setStyleSheet("color: black; background-color:white")
		self.iprps.setStyleSheet("color: black; background-color:white")
		self.idept.setStyleSheet("color: black; background-color:white")

		self.roomno.setAlignment(Qt.AlignCenter)
		self.building.setAlignment(Qt.AlignCenter)
		self.prps.setAlignment(Qt.AlignCenter)
		self.dept.setAlignment(Qt.AlignCenter)

		self.grid = QGridLayout()
		self.grid.addWidget(self.roomno,0,0)
		self.grid.addWidget(self.iroomno,0,1)
		self.grid.addWidget(self.building,1,0)
		self.grid.addWidget(self.ibuilding,1,1)
		self.grid.addWidget(self.dept,2,0)
		self.grid.addWidget(self.idept,2,1)
		self.grid.addWidget(self.prps,3,0)
		self.grid.addWidget(self.iprps,3,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")


		self.connect(self.enter,SIGNAL("clicked()"),self.entry)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)


	def setDept(self):
		self.idept.addItem("Select")
		self.idept.addItem("None")
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct Department from Doctor")
		rows = cursor.fetchall()
		db.close()
		for row in rows:
			self.idept.addItem(row[0])


	def entry(self):
		room = str(self.iroomno.text())
		building = str(self.ibuilding.text())
		dept = str(self.idept.currentText())
		prps = str(self.iprps.currentText())

		if(len(room)) == 0 or len(building)==0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","Please enter Full details. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return

		if(room.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"Error Message","Room must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(prps == "select"):
			message = QMessageBox(QMessageBox.Warning,"Error Message","Please select Purpose of this room. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if len(dept) == 0:
			dept = "NULL"

		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select RoomNo,Building from Room")
		result = self.cursor.fetchall()
		for i in range(len(result)):
			if str(result[i][0]) == room and str(result[i][1]) == building:
				message = QMessageBox(QMessageBox.Warning,"Error Message","This room in already entered. Try Again",buttons = QMessageBox.Close)
				message.exec_()
				db.close()
				return

		try:
			cursor.execute("insert into Room(RoomNo,Building,Department,Purpose) values('%s','%s','%s','%s')"%(room,building,dept,prps))
			db.commit()
			self.reset_all()
		except:
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Prescribe Message","Some error occured. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()




	def reset_all(self):
		self.iroomno.clear()
		self.ibuilding.clear()
		self.iprps.setCurrentIndex(0)
		self.idept.setCurrentIndex(0)

	def showIT(self):
		self.reset_all()
		self.show()





