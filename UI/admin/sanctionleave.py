from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb


class SanctionLeave(QFrame):

	def __init__(self):
		super(SanctionLeave,self).__init__()
		self.initUI()

	def initUI(self):
		self.table = QTableWidget()
		# self.table.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
		self.table.horizontalHeader().setResizeMode(QHeaderView.Stretch)
		self.table.resizeColumnsToContents()
		self.table.setColumnCount(4)
		self.check_box = []
		self.pid_list=[]
		self.initial_state=[]
		self.table.setHorizontalHeaderLabels(["Name","From","Till","Sanctioned"])
		self.table.verticalHeader().setVisible(False)

		self.table.setShowGrid(True)
		self.table.setStyleSheet("color: black; background-color:white")


		grid = QGridLayout()
		grid.addWidget(self.table)

		hbox = QHBoxLayout()
		hbox.addStretch(1)
		reset = QPushButton("Reset")
		enter = QPushButton("Enter")
		enter.setStyleSheet("color: black; background-color:gray")
		reset.setStyleSheet("color: black; background-color:gray")
		hbox.addWidget(enter)
		hbox.addWidget(reset)
		hbox.addStretch(1)

		vbox = QVBoxLayout()
		vbox.addLayout(grid)
		vbox.addLayout(hbox)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(vbox)

		self.connect(reset,SIGNAL("clicked()"),self.reset_all)
		self.connect(enter,SIGNAL("clicked()"),self.update)


	def reset_all(self):
		for i in range(len(self.check_box)):
			self.check_box[i].setChecked(self.initial_state[i])

	def clear_all(self):
		self.table.setRowCount(0)
		del self.check_box[:]
		del self.pid_list[:]
		del self.initial_state[:]
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select PID,Name,StartTime,EndTime,IsSanctioned from Person natural join LeaveSchedule where StartTime>now() order by PID")
		rows = cursor.fetchall()
		db.close()
		i=0
		for row in rows:
			self.table.insertRow(i)
			check = QCheckBox("Approve")
			check.setStyleSheet("color:black ; background-color:yellow")
			self.check_box.append(check)
			self.pid_list.append(row[0])
			name = QTableWidgetItem(row[1])
			start = QTableWidgetItem(row[2].date().strftime("%Y-%m-%d"))
			end = QTableWidgetItem(row[3].date().strftime("%Y-%m-%d"))
			self.table.setItem(i,0,name)
			self.table.setItem(i,1,start)
			self.table.setItem(i,2,end)
			self.table.setCellWidget(i,3,check)
			if row[4]:
				check.setChecked(True)
				self.initial_state.append(1)
			else:
				self.initial_state.append(0)
			i+=1


	def update(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		try:
			for i in range(len(self.pid_list)):
				if(self.check_box[i].isChecked()):
					cursor.execute("update LeaveSchedule set IsSanctioned=1 where PID='%d'"%(self.pid_list[i]))
				else:
					cursor.execute("update LeaveSchedule set IsSanctioned=0 where PID='%d'"%(self.pid_list[i]))
			db.commit()
		except Exception as e:
			print(e)
			db.rollback()
		db.close()





	def showIT(self):
		self.clear_all()
		self.show()


