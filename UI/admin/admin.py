from register import *
from AddFacility import *
from AddRoom import *
from sanctionleave import *
import MySQLdb


class Admin(QWidget):

	def __init__(self,parent):
		super(Admin,self).__init__()
		self.parent = parent
		self.pid = 0
		self.reg_doc = Register("doctor")
		self.reg_nurse = Register("nurse")
		self.reg_staff = Register("staff")
		self.add_facility = AddFacility()
		self.add_room = AddRoom()
		self.approve_leave = SanctionLeave()
		self.initUI()

	def initUI(self):
		self.setWindowTitle("Admin HomePage")
		self.setWindowIcon(QIcon('/home/aayush/Desktop/assign/dbms/HMS/sample3.jpg')) 	# App Icon
		self.name = QLabel()
		self.docregisterbtn = QPushButton("Register New Doctor")
		self.nurseregisterbtn = QPushButton("Register New Nurse")
		self.staffregisterbtn = QPushButton("Register New Staff")
		self.facilitybtn = QPushButton("Add Facility")
		self.roombtn = QPushButton("Add Room")
		self.approve_leavebtn = QPushButton("Approve Leave")
		self.logout = QPushButton("LogOut")
		self.temp = QTextEdit()
		self.temp.setEnabled(False)
		self.temp.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)

		self.name.setStyleSheet("color:black;")
		self.docregisterbtn.setStyleSheet("color: black; background-color:gray")
		self.nurseregisterbtn.setStyleSheet("color: black; background-color:gray")
		self.nurseregisterbtn.setStyleSheet("color: black; background-color:gray")
		self.staffregisterbtn.setStyleSheet("color: black; background-color:gray")
		self.facilitybtn.setStyleSheet("color: black; background-color:gray")
		self.roombtn.setStyleSheet("color: black; background-color:gray")
		self.approve_leavebtn.setStyleSheet("color: black; background-color:gray")
		self.logout.setStyleSheet("color: black; background-color:red")


		self.grid = QGridLayout()
		self.grid.addWidget(self.name,0,10,1,1)
		self.grid.addWidget(self.logout,0,11,1,1)
		self.grid.addWidget(self.approve_leavebtn,2,0,1,1)
		self.grid.addWidget(self.docregisterbtn,3,0,1,1)
		self.grid.addWidget(self.nurseregisterbtn,4,0,1,1)
		self.grid.addWidget(self.staffregisterbtn,5,0,1,1)
		self.grid.addWidget(self.facilitybtn,6,0,1,1)
		self.grid.addWidget(self.roombtn,7,0,1,1)
		self.grid.addWidget(self.temp,1,3,8,9)

		self.setStyleSheet("background-color:white")
		self.setLayout(self.grid)

		self.connect(self.logout,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.docregisterbtn,SIGNAL("clicked()"),self.addRegister)
		self.connect(self.nurseregisterbtn,SIGNAL("clicked()"),self.addRegister)
		self.connect(self.staffregisterbtn,SIGNAL("clicked()"),self.addRegister)
		self.connect(self.facilitybtn,SIGNAL("clicked()"),self.addFacility)
		self.connect(self.roombtn,SIGNAL("clicked()"),self.addRoom)
		self.connect(self.approve_leavebtn,SIGNAL("clicked()"),self.approveLeave)


	def buttonEvent(self):
		self.close()
		self.parent.showMaximized()


	def addRegister(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.add_room.close()
		self.add_facility.close()
		self.approve_leave.close()
		if(self.sender()==self.docregisterbtn):
			self.reg_nurse.close()
			self.reg_staff.close()
			self.grid.addWidget(self.reg_doc,1,3,8,9)
			self.reg_doc.showIT()
		elif(self.sender()==self.nurseregisterbtn):
			self.reg_doc.close()
			self.reg_staff.close()
			self.grid.addWidget(self.reg_nurse,1,3,8,9)
			self.reg_nurse.showIT()
		else:
			self.reg_doc.close()
			self.reg_nurse.close()
			self.grid.addWidget(self.reg_staff,1,3,8,9)
			self.reg_staff.showIT()



	def addFacility(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.reg_doc.close()
		self.reg_nurse.close()
		self.reg_staff.close()
		self.add_room.close()
		self.approve_leave.close()
		self.grid.addWidget(self.add_facility,1,3,8,9)
		self.add_facility.showIT()


	def addRoom(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.reg_doc.close()
		self.reg_nurse.close()
		self.reg_staff.close()
		self.add_facility.close()
		self.approve_leave.close()
		self.grid.addWidget(self.add_room,1,3,8,9)
		self.add_room.showIT()


	def approveLeave(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.reg_doc.close()
		self.reg_nurse.close()
		self.reg_staff.close()
		self.add_room.close()
		self.add_facility.close()
		self.grid.addWidget(self.approve_leave,1,3,8,9)
		self.approve_leave.showIT()


	def showIT(self,pid):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select Name from Person where PID='%s'"%(pid))
		db.close()
		rows = cursor.fetchall()
		self.pid = rows[0][0]
		self.name.setText(rows[0][0])
		self.showMaximized()
