from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb




class AddFacility(QFrame):
	def __init__(self):
		super(AddFacility,self).__init__()
		self.initUI()

	def initUI(self):

		self.name = QLabel("Name")
		self.iname = QLineEdit()
		self.iname.setPlaceholderText("Enter Name")
		self.cost = QLabel("Cost")
		self.icost = QLineEdit()
		self.icost.setPlaceholderText("Enter Cost")
		self.prps = QLabel("Purpose")
		self.iprps = QComboBox()
		self.iprps.addItem("select")
		self.iprps.addItem("operation")
		self.iprps.addItem("test")
		self.dept = QLabel("Department")
		self.idept = QComboBox()
		self.setDept()

		self.name.setFixedWidth(150)
		self.iname.setFixedWidth(300)
		self.icost.setFixedWidth(300)
		self.iprps.setFixedWidth(300)
		self.idept.setFixedWidth(300)

		self.name.setStyleSheet("color: black; background-color:gray")
		self.cost.setStyleSheet("color: black; background-color:gray")
		self.prps.setStyleSheet("color: black; background-color:gray")
		self.dept.setStyleSheet("color: black; background-color:gray")

		self.iname.setStyleSheet("color: black; background-color:white")
		self.icost.setStyleSheet("color: black; background-color:white")
		self.iprps.setStyleSheet("color: black; background-color:white")
		self.idept.setStyleSheet("color: black; background-color:white")

		self.name.setAlignment(Qt.AlignCenter)
		self.cost.setAlignment(Qt.AlignCenter)
		self.prps.setAlignment(Qt.AlignCenter)
		self.dept.setAlignment(Qt.AlignCenter)

		self.grid = QGridLayout()
		self.grid.addWidget(self.name,0,0)
		self.grid.addWidget(self.iname,0,1)
		self.grid.addWidget(self.cost,1,0)
		self.grid.addWidget(self.icost,1,1)
		self.grid.addWidget(self.dept,2,0)
		self.grid.addWidget(self.idept,2,1)
		self.grid.addWidget(self.prps,3,0)
		self.grid.addWidget(self.iprps,3,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")


		self.connect(self.enter,SIGNAL("clicked()"),self.entry)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)


	def setDept(self):
		self.idept.addItem("Select")
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct Department from Doctor")
		rows = cursor.fetchall()
		db.close()
		for row in rows:
			self.idept.addItem(row[0])


	def entry(self):
		name = str(self.iname.text())
		self.costi = str(self.icost.text())
		dept = str(self.idept.currentText())
		prps = str(self.iprps.currentText())

		if(len(name)) == 0 or len(self.costi)==0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","Please enter Full details. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if self.costi.isdigit()==0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","Please enter cost as integer value. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return

		if(prps == "select"):
			message = QMessageBox(QMessageBox.Warning,"Error Message","Please select Purpose of this facility. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if len(dept) == 0:
			dept = "NULL"

		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select Name from Facility")
		result = cursor.fetchall()
		for i in range(len(result)):
			if str(result[i][0]) == name:
				message = QMessageBox(QMessageBox.Warning,"Error Message","This Facility is already entered. Try Again",buttons = QMessageBox.Close)
				message.exec_()
				db.close()
				return

		try:
			cursor.execute("insert into Facility(Name,Cost,Department,Purpose) values('%s','%s','%s','%s')"%(name,self.costi,dept,prps))
			db.commit()
			self.reset_all()
		except Exception as e:
			print (e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Prescribe Message","Some error occured. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()


	def isFloat(self):
		try:
			float(self.costi)
			return True
		except:
			return False


	def reset_all(self):
		self.iname.clear()
		self.icost.clear()
		self.iprps.setCurrentIndex(0)
		self.idept.setCurrentIndex(0)

	def showIT(self):
		self.reset_all()
		self.show()







