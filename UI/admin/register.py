from PyQt4.QtGui import *
from PyQt4.QtCore import *
import MySQLdb


class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))


class Register(QFrame):


	def __init__(self, text):
		super(Register,self).__init__()
		self.sender = text
		self.initUI()


	def initUI(self):

		self.grid = QGridLayout()
		self.name = QLabel("Name")
		self.iname = QLineEdit()
		self.dob = QLabel("DOB")
		self.idob = extQLineEdit()
		self.gender = QLabel("Gender")
		self.igender = QComboBox()
		self.igender.addItem("male")
		self.igender.addItem("female")
		self.phone = QLabel("Phone")
		self.iphone = QLineEdit()
		self.address = QLabel("Address")
		self.iaddress = QLineEdit()
		self.DOJoin = QLabel("Date of Join")
		self.iDOJoin = extQLineEdit()
		self.salary = QLabel("Salary")
		self.isalary = QLineEdit()
		self.password = QLabel("Password")
		self.ipassword = QLineEdit()
		self.ipassword.setEchoMode(QLineEdit.Password)
		self.qualification = QLabel("Qualification")
		self.iqualification = QLineEdit()
		self.type = QLabel("Type")
		self.itype = QComboBox()
		self.itype.addItem("trainee")
		self.itype.addItem("regular")
		self.itype.addItem("visiting")
		self.dept = QLabel("Department")
		self.idept = QLineEdit()
		self.schedule = QLabel("Select Schedule")
		self.ischedule = QTableWidget(7,3)
		self.setTable()
		self.employeetype = QLabel("Select Employee type")
		self.group = QButtonGroup()
		self.imedical = QRadioButton("Medical")
		self.inonmedical = QRadioButton("Non Medical")
		self.group.addButton(self.imedical)
		self.group.addButton(self.inonmedical)
		self.facility = QLabel("Select Department")
		self.ifacility = QComboBox()
		self.setFacility()


		self.name.setStyleSheet("color: black; background-color:gray")
		self.dob.setStyleSheet("color: black; background-color:gray")
		self.gender.setStyleSheet("color: black; background-color:gray")
		self.phone.setStyleSheet("color: black; background-color:gray")
		self.address.setStyleSheet("color: black; background-color:gray")
		self.DOJoin.setStyleSheet("color: black; background-color:gray")
		self.salary.setStyleSheet("color: black; background-color:gray")
		self.password.setStyleSheet("color: black; background-color:gray")
		self.qualification.setStyleSheet("color: black; background-color:gray")
		self.type.setStyleSheet("color: black; background-color:gray")
		self.dept.setStyleSheet("color: black; background-color:gray")
		self.schedule.setStyleSheet("color: black; background-color:gray")
		self.employeetype.setStyleSheet("color: black; background-color:gray")
		self.facility.setStyleSheet("color: black; background-color:gray")

		self.iname.setStyleSheet("color: black; background-color:white")
		self.idob.setStyleSheet("color: black; background-color:white")
		self.igender.setStyleSheet("color: black; background-color:white")
		self.iphone.setStyleSheet("color: black; background-color:white")
		self.iaddress.setStyleSheet("color: black; background-color:white")
		self.iDOJoin.setStyleSheet("color: black; background-color:white")
		self.isalary.setStyleSheet("color: black; background-color:white")
		self.ipassword.setStyleSheet("color: black; background-color:white")
		self.iqualification.setStyleSheet("color: black; background-color:white")
		self.itype.setStyleSheet("color: black; background-color:white")
		self.idept.setStyleSheet("color: black; background-color:white")
		self.ischedule.setStyleSheet("color: black; background-color:white")
		self.imedical.setStyleSheet("color: black; background-color:white")
		self.inonmedical.setStyleSheet("color: black; background-color:white")
		self.ifacility.setStyleSheet("color: black; background-color:white")

		self.iname.setPlaceholderText('Enter Name')
		self.idob.setPlaceholderText('Enter Date of Birth')
		self.iphone.setPlaceholderText('Enter Phone NO.')
		self.iaddress.setPlaceholderText('Enter Address')
		self.isalary.setPlaceholderText('Enter Salary')
		self.ipassword.setPlaceholderText('Enter Password')
		self.iqualification.setPlaceholderText('Enter Qualification')
		self.idept.setPlaceholderText('Enter Department')
		self.iDOJoin.setPlaceholderText('Enter Date of Join')

		self.name.setFixedWidth(150)
		self.iname.setFixedWidth(300)
		self.idob.setFixedWidth(300)
		self.igender.setFixedWidth(300)
		self.iphone.setFixedWidth(300)
		self.iaddress.setFixedWidth(300)
		self.iDOJoin.setFixedWidth(300)
		self.isalary.setFixedWidth(300)
		self.ipassword.setFixedWidth(300)
		self.iqualification.setFixedWidth(300)
		self.idept.setFixedWidth(300)
		self.itype.setFixedWidth(300)
		self.ischedule.setFixedWidth(300)
		self.imedical.setFixedWidth(300)
		self.inonmedical.setFixedWidth(300)
		self.ifacility.setFixedWidth(300)

		self.grid.addWidget(self.name,0,0)
		self.grid.addWidget(self.iname,0,1)
		self.grid.addWidget(self.dob,1,0)
		self.grid.addWidget(self.idob,1,1)
		self.grid.addWidget(self.gender,2,0)
		self.grid.addWidget(self.igender,2,1)
		self.grid.addWidget(self.phone,3,0)
		self.grid.addWidget(self.iphone,3,1)
		self.grid.addWidget(self.address,4,0)
		self.grid.addWidget(self.iaddress,4,1)
		self.grid.addWidget(self.salary,5,0)
		self.grid.addWidget(self.isalary,5,1)
		self.grid.addWidget(self.password,6,0)
		self.grid.addWidget(self.ipassword,6,1)
		if self.sender == "doctor":
			self.grid.addWidget(self.DOJoin,7,0)
			self.grid.addWidget(self.iDOJoin,7,1)
			self.grid.addWidget(self.qualification,8,0)
			self.grid.addWidget(self.iqualification,8,1)
			self.grid.addWidget(self.dept,9,0)
			self.grid.addWidget(self.idept,9,1)
			self.grid.addWidget(self.type,10,0)
			self.grid.addWidget(self.itype,10,1)
			self.grid.addWidget(self.schedule,11,0)
			self.grid.addWidget(self.ischedule,11,1,7,1)
		elif self.sender == "staff":
			self.grid.addWidget(self.employeetype,7,0)
			self.grid.addWidget(self.imedical,7,1)
			self.grid.addWidget(self.inonmedical,8,1)
			self.grid.addWidget(self.facility,9,0)
			self.grid.addWidget(self.ifacility,9,1)



		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Register")

		self.ok.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox_xy.addStretch(1)
		self.hbox_xy.addWidget(self.reset)
		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addStretch(1)

		self.main_layout = QVBoxLayout()
		self.main_layout.addStretch(1)
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.main_layout.addStretch(1)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(self.main_layout)

		self.connect(self.idob,SIGNAL("clicked()"),self.add_dob)
		self.connect(self.iDOJoin,SIGNAL("clicked()"),self.add_join)
		self.connect(self.reset,SIGNAL("clicked()"),self.delete_all)
		self.connect(self.ok,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.imedical,SIGNAL("toggled()"),self.checkEvent)
		# self.connect(self.inonmedical,SIGNAL("toggled()"),self.checkEvent)
		self.connect(self.ifacility,SIGNAL("activated(int)"),self.findfid)


	def delete_all(self):
		self.iname.clear()
		self.iDOJoin.clear()
		self.ipassword.clear()
		self.idept.clear()
		self.iaddress.clear()
		self.iphone.clear()
		self.isalary.clear()
		self.idob.clear()
		self.iqualification.clear()
		for checkbox in self.check_box:
			checkbox[0].setChecked(False)
			checkbox[1].setChecked(False)


	def setTable(self):
		self.ischedule.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
		self.ischedule.setHorizontalHeaderLabels(["Day","Slot1(4 ours)","Slot2(4 hours)"])
		self.ischedule.verticalHeader().setVisible(False)
		self.ischedule.setCellWidget(0,0,QLabel("Monday"))
		self.ischedule.setCellWidget(1,0,QLabel("Tuesday"))
		self.ischedule.setCellWidget(2,0,QLabel("Wednesday"))
		self.ischedule.setCellWidget(3,0,QLabel("Thursday"))
		self.ischedule.setCellWidget(4,0,QLabel("Friday"))
		self.ischedule.setCellWidget(5,0,QLabel("Saturday"))
		self.ischedule.setCellWidget(6,0,QLabel("Sunday"))

		self.check_box = []

		for i in range(7):
			checkbox1 = QCheckBox("09:00:00")
			checkbox2 = QCheckBox("16:00:00")
			self.check_box.append((checkbox1,checkbox2))
			self.ischedule.setCellWidget(i,1,checkbox1)
			self.ischedule.setCellWidget(i,2,checkbox2)



	def setFacility(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select FID,Name from Facility")
		rows = cursor.fetchall()
		db.close()
		for row in rows:
			self.ifacility.addItem(row[1])



	def add_dob(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)

	def date_changed(self,date):
		date = date.toPyDate()
		self.idob.setText(str(date))
		self.cal.close()

	def add_join(self):
		self.cal = QCalendarWidget(self)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.setGridVisible(True)
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed1)

	def date_changed1(self,date):
		date = date.toPyDate()
		self.iDOJoin.setText(str(date))
		self.cal.close()

	def buttonEvent(self):
		name = str(self.iname.text())
		doj = str(self.iDOJoin.text())
		password = str(self.ipassword.text())
		dept = str(self.idept.text())
		addr = str(self.iaddress.text())
		phone = str(self.iphone.text())
		salary = str(self.isalary.text())
		dob = str(self.idob.text())
		qual = str(self.iqualification.text())
		gender = str(self.igender.currentText())
		type = str(self.itype.currentText())

		if(phone.isdigit() and len(phone)==10 and salary.isdigit()):
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor = db.cursor()
			day = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
			try:
				cursor.execute("insert into Person(DOB,Name,Gender,Address,Phone) values('%s','%s','%s','%s','%s')"%(dob,name,gender,addr,phone))
				cursor.execute("select LAST_INSERT_ID()")
				pid = cursor.fetchone()[0]
				if (self.sender=="doctor"):
					cursor.execute("insert into Doctor(PID,DOjoin,Salary,Password,Qualification,Type,Department) values('%d','%s','%d','%s','%s','%s','%s')"%(pid,doj,int(salary),password,qual,type,dept))
					cursor.execute("select LAST_INSERT_ID()")
					did = cursor.fetchone()[0]
					for i in range(7):
						if(self.check_box[i][0].isChecked()):
							cursor.execute("insert into DoctorSchedule values('%d','%s','%s','%s')"%(did,day[i],"09:00:00","13:00:00"))
						elif(self.check_box[i][1].isChecked()):
							cursor.execute("insert into DoctorSchedule values('%d','%s','%s','%s')"%(did,day[i],"16:00:00","20:00:00"))
				elif(self.sender=="nurse"):
					cursor.execute("insert into Nurse(PID,Password,Salary) values('%d','%s','%d')"%(pid,password,int(salary)))
				else:
					cursor.execute("insert into Employee(PID,Password,Salary) values('%d','%s','%d')"%(pid,password,int(salary)))
					cursor.execute("select LAST_INSERT_ID()")
					eid = cursor.fetchone()[0]
					cursor.execute("insert into WorksAt(EID,FID) values('%d','%d')"%(eid,self.fid))
				db.commit()
				message = QMessageBox(QMessageBox.Information,"Register Message","Registration Successful.Your Username is "+str(pid)+" and Password is "+password,buttons = QMessageBox.Ok)
				message.exec_()
				self.delete_all()
			except Exception as e:
				print(e)
				db.rollback()
				message = QMessageBox(QMessageBox.Warning,"Register Message","Registration Failed. Try Again",buttons = QMessageBox.Close)
				message.exec_()
			db.close()
		else:
			message = QMessageBox(QMessageBox.Warning,"Register Message","Invalid Entry.",buttons = QMessageBox.Close)
			message.exec_()


	def checkEvent(self):
		if(self.imedical.isChecked()):
			self.ifacility.setEnabled(True)
		else:
			self.ifacility.setEnabled(False)


	def findfid(self,index):
		self.fid = index+1


	def showIT(self):
		self.delete_all()
		self.show()


