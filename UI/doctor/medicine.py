from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb



class Medicine(QFrame):
	def __init__(self):
		super(Medicine,self).__init__()
		self.initUI()

	def initUI(self):

		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select distinct Name from Stock")
		result = cursor.fetchall()
		db.close()

		self.pid = 0
		self.patient = QLabel("Patient ID")
		self.patient.setAlignment(Qt.AlignCenter)
		self.ipatient = QLineEdit()
		self.ipatient.setPlaceholderText("Enter patient ID")
		self.qty = QLabel("Quantity")
		self.iqty = QLineEdit()
		self.chckup = QLabel("CheckUp ID")
		self.ichckup = QComboBox()
		self.itm = QLabel("Item Name")
		self.iitm = QComboBox()
		self.iitm.setEditable(True)
		self.iitm.setAutoCompletion(True)
		self.iitm.addItem("Select")
		for i in range(len(result)):
			self.iitm.addItem(result[i][0])
		self.iid = QLabel("Item ID")
		self.iiid = QLineEdit()
		self.iiid.setPlaceholderText("Auto-filled section")
		self.temp = QLabel("")

		self.patient.setAlignment(Qt.AlignCenter)
		self.chckup.setAlignment(Qt.AlignCenter)
		self.itm.setAlignment(Qt.AlignCenter)
		self.iid.setAlignment(Qt.AlignCenter)
		self.qty.setAlignment(Qt.AlignCenter)

		self.patient.setFixedWidth(150)
		self.ipatient.setFixedWidth(300)
		self.ichckup.setFixedWidth(300)
		self.iitm.setFixedWidth(300)
		self.iiid.setFixedWidth(300)
		self.iqty.setFixedWidth(300)

		self.patient.setStyleSheet("color: black; background-color:gray")
		self.qty.setStyleSheet("color: black; background-color:gray")
		self.chckup.setStyleSheet("color: black; background-color:gray")
		self.itm.setStyleSheet("color: black; background-color:gray")
		self.iid.setStyleSheet("color: black; background-color:gray")

		self.ipatient.setStyleSheet("color: black; background-color:white")
		self.iqty.setStyleSheet("color: black; background-color:white")
		self.ichckup.setStyleSheet("color: black; background-color:white")
		self.iitm.setStyleSheet("color: black; background-color:white")
		self.iiid.setStyleSheet("color: black; background-color:white")

		self.grid = QGridLayout()
		self.grid.addWidget(self.patient,0,0)
		self.grid.addWidget(self.ipatient,0,1)
		self.grid.addWidget(self.chckup,1,0)
		self.grid.addWidget(self.ichckup,1,1)
		self.grid.addWidget(self.itm,2,0)
		self.grid.addWidget(self.iitm,2,1)
		self.grid.addWidget(self.iid,3,0)
		self.grid.addWidget(self.iiid,3,1)
		self.grid.addWidget(self.qty,4,0)
		self.grid.addWidget(self.iqty,4,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")

		self.connect(self.iitm,SIGNAL("currentIndexChanged(const QString&)"),self.fill_iid)
		self.connect(self.enter,SIGNAL("clicked()"),self.entry)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)
		self.connect(self.ipatient,SIGNAL("returnPressed()"),self.setCheckupID)



	def setCheckupID(self):
		self.ichckup.clear()
		patID = str(self.ipatient.text())
		if(patID.isdigit()):
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor=db.cursor()
			cursor.execute("select DID from Doctor where PID = '%d'"%(int(self.pid)))
			did = cursor.fetchone()[0]
			cursor.execute("select CID from CheckUp where PatID = '%d' and DID='%d'"%(int(patID),did))
			result = cursor.fetchall()
			db.close()
			for res in result:
				self.ichckup.addItem(str(res[0]))
		else:
			message = QMessageBox(QMessageBox.Warning,"Message","Incorrect Patient ID entered.",buttons = QMessageBox.Close)
			message.exec_()


	def entry(self):
		pid = str(self.ipatient.text())
		cid = str(self.ichckup.text())
		itemid = str(self.iiid.text())
		qty = str(self.iqty.text())

		if len(pid) == 0 or len(cid) == 0 or len(itemid) == 0 or len(qty) == 0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","All fields not entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(pid.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"PID Message","PID must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(cid.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"CID Message","CID must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(qty.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"Quantity Message","Quantity must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return

		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select CID,PatID from CheckUp where PatID = %s",(pid))
		result = self.cursor.fetchall()
		if(len(result)) == 0:
			message = QMessageBox(QMessageBox.Warning,"PID Message","Incorrect Patient ID entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		for i in range(len(result)):
			if str(result[i][0]) == cid:
				try:
					cursor.execute("insert into GiveMedicines(CID,ItemID,Quantity) values('%s','%s','%s')"%(cid,itemid,qty))
					db.commit()
					self.iitm.setCurrentIndex(0)
					self.iiid.clear()
					self.iqty.clear()
				except:
					db.rollback()
					message = QMessageBox(QMessageBox.Warning,"Prescribe Medicine","Some error occured. Try Again",buttons = QMessageBox.Close)
					message.exec_()
				break
			if i==len(result)-1:
				message = QMessageBox(QMessageBox.Warning,"CID Message","Incorrect CheckUp ID entered. Try Again",buttons = QMessageBox.Close)
				message.exec_()
		db.close()



	def reset_all(self):
		self.ipatient.clear()
		self.ichckup.clear()
		self.iitm.setCurrentIndex(0)
		self.iiid.clear()
		self.iqty.clear()

	def fill_iid(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select Name,ItemID from Stock")
		result = cursor.fetchall()
		db.close()
		for i in range(len(result)):
			if result[i][0] == str(self.iitm.currentText()):
				self.iiid.setText(str(result[i][1]))
				break


	def showIT(self,pid):
		self.pid = pid
		self.reset_all()
		self.show()




# app = QApplication(sys.argv)
# start = Medicine()
# start.show()
# app.exec_()