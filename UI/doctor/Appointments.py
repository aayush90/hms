__author__ = 'batman'

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import MySQLdb as mdb
import datetime

class Appointments(QFrame):
	def __init__(self):
		super(Appointments, self).__init__()
		self.myID = 0
		self.name = ""
		self.qualification = ""
		self.dept = ""
		self.initAction()
		self.init_layout()

	def initAction(self):

		self.nameLabel = QLabel('Name:')
		self.nameLabel.setAlignment(Qt.AlignCenter)
		self.qualLabel = QLabel('Qualification:')
		self.qualLabel.setAlignment(Qt.AlignCenter)
		self.deptLabel = QLabel('Department:')
		self.deptLabel.setAlignment(Qt.AlignCenter)

		self.nameLabel.setStyleSheet("color: black; background-color:gray")
		self.qualLabel.setStyleSheet("color: black; background-color:gray")
		self.deptLabel.setStyleSheet("color: black; background-color:gray")

		self.nameText = QLineEdit()
		self.qualText = QLineEdit()
		self.deptText = QLineEdit()

		self.nameText.setStyleSheet("color: black; background-color:white")
		self.qualText.setStyleSheet("color: black; background-color:white")
		self.deptText.setStyleSheet("color: black; background-color:white")

		columns = ['PatientName', 'IsEmergency', 'IsHomeVisit', 'AppointmentTime']
		self.table = QTableWidget()
		self.table.setColumnCount(4)
		self.table.setHorizontalHeaderLabels(columns)
		self.table.setSortingEnabled(True)
		self.table.horizontalHeader().setResizeMode(QHeaderView.Stretch)
		self.table.resizeColumnsToContents()
		self.table.setShowGrid(True)

		self.table.setStyleSheet("color: black; background-color:white")

		self.printButton = QPushButton('Done')
		self.connect(self.printButton,SIGNAL('clicked()'),self.saveInfo)
		self.updateButton = QPushButton('Update')
		self.connect(self.printButton,SIGNAL('clicked()'),self.updateInfo)

		self.printButton.setStyleSheet("color: black; background-color:lightgray")
		self.updateButton.setStyleSheet("color: black; background-color:lightgray")

	def init_layout(self):
		self.p_layout = QGridLayout(self)
		temp = QLabel()
		temp.setFixedHeight(20)
		self.p_layout.addWidget(temp,0,0,1,-1)

		temp1 = QLabel()
		temp1.setFixedHeight(15)
		self.p_layout.addWidget(temp1,1,0,1,-1)

		self.p_layout.addWidget(self.nameLabel,2,0,1,1)
		self.p_layout.addWidget(self.nameText,2,1,1,1)
		self.p_layout.addWidget(self.qualLabel,2,2,1,1)
		self.p_layout.addWidget(self.qualText,2,3,1,1)
		self.p_layout.addWidget(self.deptLabel,2,4,1,1)
		self.p_layout.addWidget(self.deptText,2,5,1,1)

		temp2 = QLabel()
		temp2.setFixedHeight(15)
		self.p_layout.addWidget(temp2,3,0,1,-1)
		self.p_layout.addWidget(self.table,3,0,4,-1)
		self.p_layout.addWidget(self.printButton,7,5,1,1)
		self.p_layout.addWidget(self.updateButton,7,4,1,1)

		self.setStyleSheet("background-color:lightblue;")


	def getAppointments(self):
		self.appDic = []

		try:
			f = '%Y-%m-%d %H:%M:%S'
			# db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()
			sql = 'SELECT PatID,IsEmergency,IsHomeVisit,AppointmentTime FROM PatientEntry WHERE DID = %s AND AppointmentTime >= %s'
			cur.execute(sql,(self.myID,datetime.datetime.now().strftime(f)))
			data = cur.fetchall()
			for i in data:
				dic = {}
				cur.execute('SELECT DISTINCT Name FROM Person WHERE PID IN (SELECT DISTINCT PID FROM Patient WHERE PatID = %s)',(i[0]))
				dic['PatientName'] = str(cur.fetchall()[0][0])
				dic['IsEmergency'] = str(i[1])
				dic['IsHomeVisit'] = str(i[2])
				dic['AppointmentTime'] = str(i[3])
				self.appDic.append(dic)

			db.close()
		except:
			pass


	def viewAppointments(self):
		columns = ['PatientName', 'IsEmergency', 'IsHomeVisit', 'AppointmentTime']
		l = len(self.appDic)
		self.table.setRowCount(l)
		for i in range(l):
			for j in range(4):
				it = QTableWidgetItem()
				it.setFlags(it.flags() & ~Qt.ItemIsEditable)
				text = self.appDic[i][columns[j]]
				it.setText(text)
				self.table.setItem(i,j,it)


	def showMyDetails(self):
		self.nameText.setText('Dr '+self.name)
		self.qualText.setText(self.qualification)
		self.deptText.setText(self.dept)


	def saveInfo(self):
		pass


	def updateInfo(self):
		pass


	def showIT(self,pid):
		# db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select Name from Person where PID='%d'"%(pid))
		self.name = cursor.fetchone()[0]
		cursor.execute("select DID,Qualification,Department from Doctor where PID='%d'"%(pid))
		row =  cursor.fetchone()
		self.myID = row[0]
		self.qualification = row[1]
		self.dept = row[2]
		self.showMyDetails()
		self.getAppointments()
		self.viewAppointments()
		self.show()
