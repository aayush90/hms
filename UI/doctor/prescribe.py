from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb


class Prescribe(QFrame):
	def __init__(self):
		super(Prescribe,self).__init__()
		self.initUI()

	def initUI(self):

		self.pid = 0
		self.patient = QLabel("Patient ID")
		self.ipatient = QLineEdit()
		self.ipatient.setPlaceholderText("Enter patient ID")
		self.chckup = QLabel("CheckUp ID")
		self.ichckup = QComboBox()
		self.prps = QLabel("Purpose")
		self.iprps = QComboBox()
		self.iprps.addItem("select")
		self.iprps.addItem("operation")
		self.iprps.addItem("test")
		self.faclty = QLabel("Facility")
		self.ifaclty = QComboBox()
		self.ifaclty.setEditable(True)
		self.ifaclty.setAutoCompletion(True)
		self.fid = QLabel("FID")
		self.ifid = QLineEdit()
		self.ifid.setEnabled(False)
		self.ifid.setPlaceholderText("Auto-filled section")

		self.patient.setFixedWidth(150)
		self.ipatient.setFixedWidth(300)
		self.ichckup.setFixedWidth(300)
		self.iprps.setFixedWidth(300)
		self.ifaclty.setFixedWidth(300)
		self.ifid.setFixedWidth(300)

		self.patient.setStyleSheet("color: black; background-color:gray")
		self.chckup.setStyleSheet("color: black; background-color:gray")
		self.prps.setStyleSheet("color: black; background-color:gray")
		self.faclty.setStyleSheet("color: black; background-color:gray")
		self.fid.setStyleSheet("color: black; background-color:gray")

		self.ipatient.setStyleSheet("color: black; background-color:white")
		self.ichckup.setStyleSheet("color: black; background-color:white")
		self.iprps.setStyleSheet("color: black; background-color:white")
		self.ifaclty.setStyleSheet("color: black; background-color:white")
		self.ifid.setStyleSheet("color: black; background-color:white")

		self.patient.setAlignment(Qt.AlignCenter)
		self.chckup.setAlignment(Qt.AlignCenter)
		self.prps.setAlignment(Qt.AlignCenter)
		self.fid.setAlignment(Qt.AlignCenter)
		self.faclty.setAlignment(Qt.AlignCenter)

		self.grid = QGridLayout()
		self.grid.addWidget(self.patient,0,0)
		self.grid.addWidget(self.ipatient,0,1)
		self.grid.addWidget(self.chckup,1,0)
		self.grid.addWidget(self.ichckup,1,1)
		self.grid.addWidget(self.prps,2,0)
		self.grid.addWidget(self.iprps,2,1)
		self.grid.addWidget(self.faclty,3,0)
		self.grid.addWidget(self.ifaclty,3,1)
		self.grid.addWidget(self.fid,4,0)
		self.grid.addWidget(self.ifid,4,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")

		self.connect(self.iprps,SIGNAL("activated(QString)"),self.fill_name)
		self.connect(self.ifaclty,SIGNAL("activated(QString)"),self.fill_fid)
		self.connect(self.enter,SIGNAL("clicked()"),self.entry)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)
		self.connect(self.ipatient,SIGNAL("returnPressed()"),self.setCheckupID)



	def setCheckupID(self):
		self.ichckup.clear()
		patID = str(self.ipatient.text())
		if(patID.isdigit()):
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor=db.cursor()
			cursor.execute("select DID from Doctor where PID = '%d'"%(int(self.pid)))
			did = cursor.fetchone()[0]
			cursor.execute("select CID from CheckUp where PatID = '%d' and DID='%d'"%(int(patID),did))
			result = cursor.fetchall()
			db.close()
			for res in result:
				self.ichckup.addItem(str(res[0]))
		else:
			message = QMessageBox(QMessageBox.Warning,"Message","Incorrect Patient ID entered.",buttons = QMessageBox.Close)
			message.exec_()



	def entry(self):
		pid = str(self.ipatient.text())
		cid = str(self.ichckup.text())
		fid = str(self.ifid.text())
		if len(pid) == 0 or len(cid) == 0 or len(fid) == 0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","All fields not entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(pid.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"PID Message","PID must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(cid.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"CID Message","CID must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return

		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select CID,PatID from CheckUp where PatID = '%d'"%(int(pid)))
		result = cursor.fetchall()
		if(len(result)) == 0:
			message = QMessageBox(QMessageBox.Warning,"PID Message","Incorrect Patient ID entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			db.close()
			return
		for i in range(len(result)):
			if str(result[i][0]) == cid:
				try:
					cursor.execute("insert into PrescribeTestOrOperation(CID,FID) values('%s','%s')"%(cid,fid))
					db.commit()
					self.reset_all()
				except:
					db.rollback()
					message = QMessageBox(QMessageBox.Warning,"Prescribe Message","Some error occured. Try Again",buttons = QMessageBox.Close)
					message.exec_()
				break
			if i==len(result)-1:
				message = QMessageBox(QMessageBox.Warning,"CID Message","Incorrect CheckUp ID entered. Try Again",buttons = QMessageBox.Close)
				message.exec_()
		db.close()




	def reset_all(self):
		self.ipatient.clear()
		self.ichckup.clear()
		self.iprps.setCurrentIndex(0)
		self.ifaclty.clear()


	def fill_name(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select Name,Purpose,FID from Facility")
		result = cursor.fetchall()
		db.close()
		self.ifaclty.clear()
		if str(self.iprps.currentText() == "select"):
			self.ifid.clear()
		for i in range(len(result)):
			if result[i][1] == str(self.iprps.currentText()):
				self.ifaclty.addItem(result[i][0])


	def fill_fid(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select Name,Purpose,FID from Facility")
		result = cursor.fetchall()
		db.close()
		for i in range(len(result)):
			if result[i][0] == str(self.ifaclty.currentText()):
				self.ifid.setText(str(result[i][2]))
				break


	def showIT(self,pid):
		self.pid = pid
		self.reset_all()
		self.show()




