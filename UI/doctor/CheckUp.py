from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb



class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))

class CheckUp(QFrame):
	def __init__(self):
		super(CheckUp,self).__init__()
		self.initUI()

	def initUI(self):

		self.did = 0
		self.patient = QLabel("Patient ID")
		self.ipatient = QLineEdit()
		self.ipatient.setPlaceholderText("Enter patient ID")
		self.chcktime = QLabel("CheckUp Time")
		self.ichcktime = QLineEdit()
		self.ichcktime.setPlaceholderText("CheckUp Time HH:MM:SS(24 hour)")
		self.chckday = QLabel("CheckUp Date")
		self.ichckday = extQLineEdit()
		self.ichckday.setPlaceholderText("Enter CheckUp Date")
		self.diagnosis = QLabel("Diagnosis")
		self.idiagnosis = QLineEdit()
		self.idiagnosis.setPlaceholderText("Enter Diagnosis")
		self.treatment = QLabel("Treatment")
		self.itreatment = QLineEdit()
		self.itreatment.setPlaceholderText("Enter Treatment")
		self.tobeadmit = QLabel("To Be Admitted")
		self.itobeadmit = QComboBox()
		self.itobeadmit.addItem("Select")
		self.itobeadmit.addItem("Yes")
		self.itobeadmit.addItem("No")

		self.patient.setFixedWidth(150)
		self.ipatient.setFixedWidth(300)
		self.ichckday.setFixedWidth(300)
		self.ichcktime.setFixedWidth(300)
		self.idiagnosis.setFixedWidth(300)
		self.itreatment.setFixedWidth(300)
		self.itobeadmit.setFixedWidth(300)

		self.patient.setStyleSheet("color: black; background-color:gray")
		self.chcktime.setStyleSheet("color: black; background-color:gray")
		self.chckday.setStyleSheet("color: black; background-color:gray")
		self.diagnosis.setStyleSheet("color: black; background-color:gray")
		self.treatment.setStyleSheet("color: black; background-color:gray")
		self.tobeadmit.setStyleSheet("color: black; background-color:gray")

		self.ipatient.setStyleSheet("color: black; background-color:white")
		self.ichcktime.setStyleSheet("color: black; background-color:white")
		self.ichckday.setStyleSheet("color: black; background-color:white")
		self.idiagnosis.setStyleSheet("color: black; background-color:white")
		self.itreatment.setStyleSheet("color: black; background-color:white")
		self.itobeadmit.setStyleSheet("color: black; background-color:white")

		self.patient.setAlignment(Qt.AlignCenter)
		self.chcktime.setAlignment(Qt.AlignCenter)
		self.chckday.setAlignment(Qt.AlignCenter)
		self.diagnosis.setAlignment(Qt.AlignCenter)
		self.treatment.setAlignment(Qt.AlignCenter)
		self.tobeadmit.setAlignment(Qt.AlignCenter)

		self.grid = QGridLayout()
		self.grid.addWidget(self.patient,0,0)
		self.grid.addWidget(self.ipatient,0,1)
		self.grid.addWidget(self.chckday,1,0)
		self.grid.addWidget(self.ichckday,1,1)
		self.grid.addWidget(self.chcktime,2,0)
		self.grid.addWidget(self.ichcktime,2,1)
		self.grid.addWidget(self.diagnosis,3,0)
		self.grid.addWidget(self.idiagnosis,3,1)
		self.grid.addWidget(self.treatment,4,0)
		self.grid.addWidget(self.itreatment,4,1)
		self.grid.addWidget(self.tobeadmit,5,0)
		self.grid.addWidget(self.itobeadmit,5,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:lightgray")
		self.reset.setStyleSheet("color: black; background-color:lightgray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")

		self.connect(self.ichckday,SIGNAL("clicked()"),self.add_date)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)
		self.connect(self.enter,SIGNAL("clicked()"),self.entry)


	def add_date(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)

	def date_changed(self,date):
		date = date.toPyDate()
		self.ichckday.setText(str(date))
		self.cal.close()

	def entry(self):
		pid = str(self.ipatient.text())
		chcktime = ""
		chcktime += str(self.ichckday.text())
		chcktime += " "
		chcktime += str(self.ichcktime.text())
		diag = str(self.idiagnosis.text())
		trea = str(self.itreatment.text())
		if str(self.itobeadmit.currentText()) == "Yes":
			admit = 1
		else:
			admit = 0
		if diag == "":
			diag = "NULL"
		if trea == "":
			trea = "NULL"
		if len(str(self.ichckday.text())) == 0 or len(pid) == 0 or len(str(self.ichcktime.text())) == 0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","All fields not entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if len(str(self.ichcktime.text()))!=8:
			message = QMessageBox(QMessageBox.Warning,"Error Message","Error in Time. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if(pid.isdigit() == 0):
			message = QMessageBox(QMessageBox.Warning,"PID Message","PID must be an integer. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select PatID from Patient")
		result = self.cursor.fetchall()
		for i in range(len(result)):
			if(str(result[i][0]) == pid):
				break
			if i == len(result) -1:
				message = QMessageBox(QMessageBox.Warning,"PID Message","Incorrect PID. Try Again",buttons = QMessageBox.Close)
				message.exec_()
				self.ipatient.clear()
				db.close()
				return

		try:
			cursor.execute("insert into CheckUp(PatID,DID,CheckUpTime,Diagnosis,Treatment,tobeAdmitted) values('%s','%s','%s','%s','%s','%s')"%(pid,str(self.did),chcktime,diag,trea,str(admit)))
			db.commit()
			self.reset_all()
		except Exception as e:
			print (e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Error Message","Error Occured. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()




	def reset_all(self):
		self.ipatient.clear()
		self.ichckday.clear()
		self.ichcktime.clear()
		self.idiagnosis.clear()
		self.itreatment.clear()
		self.itobeadmit.setCurrentIndex(0)


	def setDocID(self,pid):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select DID from Doctor where PID = %s",pid)
		did = cursor.fetchall()
		db.close()
		self.did = did[0]


	def showIT(self,pid):
		self.setDocID(pid)
		self.reset_all()
		self.show()




