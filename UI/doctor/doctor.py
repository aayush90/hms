from prescribe import *
from CheckUp import *
from medicine import *
from UI.ApplyLeave import *
from Appointments import *
import MySQLdb


class Doctor(QWidget):

	def __init__(self,parent):
		super(Doctor,self).__init__()
		self.parent = parent
		self.prescribe = Prescribe()
		self.checkup = CheckUp()
		self.medicine = Medicine()
		self.leave = Leave()
		self.appointment = Appointments()
		self.pid = 0
		self.initUI()

	def initUI(self):
		self.setWindowTitle("Doctor HomePage")
		self.setWindowIcon(QIcon('/home/aayush/Desktop/assign/dbms/HMS/sample3.jpg')) 	# App Icon
		self.name = QLabel()
		self.leavebtn = QPushButton("Apply for Leave")
		self.prescribebtn = QPushButton("Give Prescription")
		self.checkupbtn = QPushButton("Insert CheckUp Details")
		self.medicinebtn = QPushButton("Medicine Prescription")
		self.appointmentbtn = QPushButton("View Personal Information")
		self.logout = QPushButton("LogOut")
		self.temp = QTextEdit()
		self.temp.setEnabled(False)
		self.temp.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)

		self.name.setStyleSheet("color:black;")
		self.leavebtn.setStyleSheet("color: black; background-color:gray")
		self.prescribebtn.setStyleSheet("color: black; background-color:gray")
		self.medicinebtn.setStyleSheet("color: black; background-color:gray")
		self.checkupbtn.setStyleSheet("color: black; background-color:gray")
		self.appointmentbtn.setStyleSheet("color: black; background-color:gray")
		self.logout.setStyleSheet("color: black; background-color:red")

		self.grid = QGridLayout()
		self.grid.addWidget(self.name,0,10,1,1)
		self.grid.addWidget(self.logout,0,11,1,1)
		self.grid.addWidget(self.leavebtn,2,0,1,1)
		self.grid.addWidget(self.prescribebtn,3,0,1,1)
		self.grid.addWidget(self.checkupbtn,4,0,1,1)
		self.grid.addWidget(self.medicinebtn,5,0,1,1)
		self.grid.addWidget(self.appointmentbtn,6,0,1,1)
		self.grid.addWidget(self.temp,1,3,7,9)

		self.setLayout(self.grid)
		self.setStyleSheet("background-color:white")

		self.connect(self.logout,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.prescribebtn,SIGNAL("clicked()"),self.givePrescription)
		self.connect(self.checkupbtn,SIGNAL("clicked()"),self.insertCheckUpDetails)
		self.connect(self.medicinebtn,SIGNAL("clicked()"),self.prescribeMedicine)
		self.connect(self.leavebtn,SIGNAL("clicked()"),self.applyLeave)
		self.connect(self.appointmentbtn,SIGNAL("clicked()"),self.viewInformation)


	def givePrescription(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.checkup.close()
		self.medicine.close()
		self.leave.close()
		self.appointment.close()
		self.grid.addWidget(self.prescribe,1,3,7,9)
		self.prescribe.showIT(self.pid)


	def insertCheckUpDetails(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.prescribe.close()
		self.medicine.close()
		self.leave.close()
		self.appointment.close()
		self.grid.addWidget(self.checkup,1,3,7,9)
		self.checkup.showIT(self.pid)


	def prescribeMedicine(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.prescribe.close()
		self.checkup.close()
		self.leave.close()
		self.appointment.close()
		self.grid.addWidget(self.medicine,1,3,7,9)
		self.medicine.showIT(self.pid)



	def applyLeave(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.prescribe.close()
		self.checkup.close()
		self.medicine.close()
		self.appointment.close()
		self.grid.addWidget(self.leave,1,3,7,9)
		self.leave.showIT(self.pid)


	def viewInformation(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.prescribe.close()
		self.checkup.close()
		self.medicine.close()
		self.leave.close()
		self.grid.addWidget(self.appointment,1,3,7,9)
		self.appointment.showIT(self.pid)


	def buttonEvent(self):
		self.close()
		self.parent.showMaximized()

	def showIT(self,pid):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select Name from Person where PID='%d'"%(pid))
		rows = cursor.fetchall()
		db.close()
		self.pid = pid
		self.name.setText(rows[0][0])
		self.showMaximized()




