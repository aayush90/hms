from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb

class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))

class Leave(QFrame):
	def __init__(self):
		super(Leave,self).__init__()
		self.initUI()
		self.pid = 0

	def initUI(self):
		self.stday = QLabel("Start Date")
		self.istday = extQLineEdit()
		self.istday.setPlaceholderText("Enter Start Date")
		self.endday = QLabel("End Date")
		self.iendday = extQLineEdit()
		self.iendday.setPlaceholderText("Enter End Date")

		self.stday.setFixedWidth(150)
		self.istday.setFixedWidth(300)
		self.iendday.setFixedWidth(300)

		self.stday.setStyleSheet("color: black; background-color:gray")
		self.endday.setStyleSheet("color: black; background-color:gray")

		self.istday.setStyleSheet("color: black; background-color:white")
		self.iendday.setStyleSheet("color: black; background-color:white")

		self.stday.setAlignment(Qt.AlignCenter)
		self.endday.setAlignment(Qt.AlignCenter)


		self.grid = QGridLayout()
		self.grid.addWidget(self.stday,0,0)
		self.grid.addWidget(self.istday,0,1)
		self.grid.addWidget(self.endday,1,0)
		self.grid.addWidget(self.iendday,1,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")

		self.connect(self.istday,SIGNAL("clicked()"),self.add_date)
		self.connect(self.iendday,SIGNAL("clicked()"),self.add_date1)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)
		self.connect(self.enter,SIGNAL("clicked()"),self.entry)

	def reset_all(self):
		self.istday.clear()
		self.iendday.clear()

	def date_changed(self,date):
		date = date.toPyDate()
		self.istday.setText(str(date))
		self.cal.close()

	def add_date(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.setMinimumDate(QDate.currentDate())
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)



	def add_date1(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.setMinimumDate(QDate.currentDate())
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed1)

	def date_changed1(self,date):
		date = date.toPyDate()
		self.iendday.setText(str(date))
		self.cal.close()


	def entry(self):
		sttime = str(self.istday.text())
		sttime += " 00:00:00"
		endtime = str(self.iendday.text())
		endtime += " 00:00:00"

		if len(str(self.istday.text())) == 0 or len(str(self.iendday.text())) == 0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","All fields not entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		try:
			cursor.execute("insert into LeaveSchedule(PID,StartTime,EndTime) values('%s','%s','%s')"%(self.pid,sttime,endtime))
			db.commit()
			self.reset_all()
		except Exception as e:
			print (e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Error Message","Error Occured. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()



	def showIT(self,pid):
		self.pid = pid
		self.reset_all()
		self.show()

