from PyQt4.QtCore import *
from checkValid import *
from datetime import datetime


class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))


class Admit(QFrame):

	def __init__(self):
		super(Admit,self).__init__()
		self.initUI()

	def initUI(self):

		self.patId = QLabel("Patient Id")
		self.ipatId = QLineEdit()
		self.doa = QLabel("Date of Test")
		self.idoa = extQLineEdit()
		self.docID = QLabel("Doctor Name")
		self.idocID = QLineEdit()
		self.ward = QLabel("Ward Type")
		self.iward = QComboBox()
		self.iward.addItem("general")
		self.iward.addItem("private")
		self.iward.addItem("icu")
		self.mode = QLabel("Mode")
		self.imode = QComboBox()
		self.imode.addItem("Cash")
		self.imode.addItem("Cheque")
		self.amount = QLabel("Amount")
		self.iamount = QLineEdit()
		self.iamount.setEnabled(False)

		self.ipatId.setPlaceholderText('Enter Patient ID')
		self.idoa.setPlaceholderText('Enter Date of Test')
		self.idocID.setPlaceholderText('Enter Doctor ID')
		self.iamount.setPlaceholderText('Amount(in Rs)')


		self.ward.setStyleSheet("color: black; background-color:gray")
		self.patId.setStyleSheet("color: black; background-color:gray")
		self.doa.setStyleSheet("color: black; background-color:gray")
		self.docID.setStyleSheet("color: black; background-color:gray")
		self.mode.setStyleSheet("color: black; background-color:gray")
		self.amount.setStyleSheet("color: black; background-color:gray")

		self.ward.setAlignment(Qt.AlignCenter)
		self.patId.setAlignment(Qt.AlignCenter)
		self.doa.setAlignment(Qt.AlignCenter)
		self.docID.setAlignment(Qt.AlignCenter)
		self.amount.setAlignment(Qt.AlignCenter)
		self.mode.setAlignment(Qt.AlignCenter)

		self.iward.setStyleSheet("color: black; background-color:white")
		self.ipatId.setStyleSheet("color: black; background-color:white")
		self.idoa.setStyleSheet("color: black; background-color:white")
		self.idocID.setStyleSheet("color: black; background-color:white")
		self.imode.setStyleSheet("color: black; background-color:white")
		self.iamount.setStyleSheet("color: black; background-color:white")

		self.patId.setFixedWidth(150)
		self.ipatId.setFixedWidth(300)
		self.iward.setFixedWidth(300)
		self.idoa.setFixedWidth(300)
		self.idocID.setFixedWidth(300)
		self.imode.setFixedWidth(300)
		self.iamount.setFixedWidth(300)

		self.grid = QGridLayout()
		self.grid.addWidget(self.patId,0,0)
		self.grid.addWidget(self.ipatId,0,1)
		self.grid.addWidget(self.docID,1,0)
		self.grid.addWidget(self.idocID,1,1)
		self.grid.addWidget(self.doa,2,0)
		self.grid.addWidget(self.idoa,2,1)
		self.grid.addWidget(self.ward,3,0)
		self.grid.addWidget(self.iward,3,1)
		self.grid.addWidget(self.mode,4,0)
		self.grid.addWidget(self.imode,4,1)
		self.grid.addWidget(self.amount,5,0)
		self.grid.addWidget(self.iamount,5,1)




		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Enter")

		self.ok.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox_xy.addStretch(1)
		self.hbox_xy.addWidget(self.reset)
		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addStretch(1)

		self.main_layout = QVBoxLayout()
		self.main_layout.addStretch(1)
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.main_layout.addStretch(1)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(self.main_layout)

		self.connect(self.reset,SIGNAL("clicked()"),self.delete_all)
		self.connect(self.ok,SIGNAL("clicked()"),self.enter)
		self.connect(self.idoa,SIGNAL("clicked()"),self.setDOA)
		self.connect(self.iward,SIGNAL("activated(QString)"),self.setAmount)


	def setDOA(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)

	def date_changed(self,date):
		date = date.toPyDate()
		self.idoa.setText(str(date))
		self.cal.close()


	def setAmount(self,type):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct Rate from AdmitRoom where Type='%s'"%(str(type)))
		row = cursor.fetchone()
		self.iamount.setText(str(row[0]))



	def delete_all(self):
		self.idoa.clear()
		self.idocID.clear()
		self.ipatId.clear()
		self.iamount.clear()


	def enter(self):
		docID = str(self.idocID.text())
		patID = str(self.patId)
		date = datetime.now().date().strftime("%Y-%m-%d")
		mode = str(self.imode.currentText())
		amount = str(self.iamount.text())
		if(docID.isdigit() and patID.isdigit()):
			details = admitInfoInsert(int(docID),int(patID),str(self.iward.currentText()))
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor = db.cursor()
			try:
				cursor.execute("insert into Accounts(DOpayment,Mode,Purpose,Amount) values('%s','%s','%s','%f')"%(date,mode,"admit",float(amount)))
				db.commit()
				message = QMessageBox(QMessageBox.Information,"Patient Admit","Patient Admitted successfully.Your Room No is "+str(details["RoomNo"])+" in Building "+details["Building"],buttons = QMessageBox.Ok)
				message.exec_()
				self.delete_all()
			except Exception as e:
				db.rollback()
				print(e)
			db.close()
		else:
			message = QMessageBox(QMessageBox.Warning,"Room Allotment","Invalid Entry. Try Again",buttons = QMessageBox.Close)
			message.exec_()


	def showIT(self):
		self.delete_all()
		self.show()

