from PyQt4.QtGui import *
import MySQLdb
import datetime


def checkValidTestOrOperation(DID,PatID,FID):
	db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
	cursor=db.cursor()
	cursor.execute("select max(CID) from CheckUp where DID='%d' and PatID='%d'"%(DID,PatID))
	tpl=cursor.fetchone()
	if tpl is None:
		db.close()
		return 0
	CID=tpl[0]
	cursor.execute("select count(*) from PrescribeTestOrOperation where CID='%d' and FID='%d' " %(CID,FID))
	tpl=cursor.fetchone()
	if tpl[0]==0:
		db.close()
		return 0
	db.close()
	return 1

def checkValidAdmit(DID,PatID):
	db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
	cursor=db.cursor()
	cursor.execute("select max(CID) from CheckUp where DID='%d' and PatID='%d'"%(DID,PatID))
	tpl=cursor.fetchone()
	if tpl is None:
		db.close()
		return 0
	cursor.execute("select tobeAdmitted from CheckUp where CID='%d'"%(tpl[0]))
	tpl=cursor.fetchone()
	if tpl[0] is None:
		db.close()
		return 0
	if tpl[0]==0:
		db.close()
		return  0
	db.close()
	return 1

def admitInfoInsert(DID,PatID,WardType):
	try:
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		dic={}
		dic['Error']=''
		if checkValidAdmit(DID,PatID)==0:
			dic['Error']='To Admit is not prescribed by the doctor'
			db.close()
			return dic
		cursor.execute("select RoomNo,Building from AdmitRoom where Type='%s' and Capacity>No_of_pat limit 1"%(WardType))
		rn=cursor.fetchone()
		if rn is None:
			dic['Error']='Room of Given Type not available'
			db.close()
			return dic
		cursor.execute("select count(*) from AdmitInfo where PatID='%d'"%(PatID))
		num=cursor.fetchone()[0]
		if num==0:
			sql="""select N from (select NID N, (select count(*) from AdmitInfo where NID=N) CT from Nurse) T
			where T.CT=(select min(CT)
			from (select NID N, (select count(*) from AdmitInfo where NID=N) CT from Nurse) T1) limit 1"""
			cursor.execute(sql)
			nid=cursor.fetchone()[0]
			dt=datetime.datetime.now()
			dt=str(dt.year)+'-'+str(dt.month)+'-'+str(dt.day)
			cursor.execute("insert into AdmitInfo values('%d','%d','%s','%s','%s')"%(PatID,nid,dt,rn[0],rn[1]))
			dic['NID']=nid
			dic['RoomNo']=rn[0]
			dic['Building']=rn[1]
			cursor.execute("update AdmitRoom set No_of_pat=No_of_pat+1 where RoomNo='%s' and Building='%s'"%(rn[0],rn[1]))
			db.commit()
			db.close()
			return dic
		cursor.execute("select NID,RoomNo,Building from AdmitInfo where PatID='%d' limit 1"%(PatID))
		tpl=cursor.fetchone()
		dic['NID']=tpl[0]
		dic['RoomNo']=tpl[1]
		dic['Building']=tpl[2]
		db.close()
		return  dic
	except Exception as e:
		print(e)
		db.rollback()
		message = QMessageBox(QMessageBox.Warning,"Room Allotment","Admit Room Allotment Failed. Try Again",buttons = QMessageBox.Close)
		message.exec_()



