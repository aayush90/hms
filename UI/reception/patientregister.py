from PyQt4.QtGui import *
from PyQt4.QtCore import *
import MySQLdb



class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))


class PatientRegister(QFrame):


	def __init__(self):
		super(PatientRegister,self).__init__()
		self.initUI()


	def initUI(self):

		self.grid = QGridLayout()
		self.name = QLabel("Name")
		self.iname = QLineEdit()
		self.dob = QLabel("DOB")
		self.idob = extQLineEdit()
		self.gender = QLabel("Gender")
		self.igender = QComboBox()
		self.igender.addItem("male")
		self.igender.addItem("female")
		self.phone = QLabel("Phone")
		self.iphone = QLineEdit()
		self.address = QLabel("Address")
		self.iaddress = QLineEdit()
		self.password = QLabel("Password")
		self.ipassword = QLineEdit()
		self.ipassword.setEchoMode(QLineEdit.Password)

		self.name.setStyleSheet("color: black; background-color:gray")
		self.dob.setStyleSheet("color: black; background-color:gray")
		self.gender.setStyleSheet("color: black; background-color:gray")
		self.phone.setStyleSheet("color: black; background-color:gray")
		self.address.setStyleSheet("color: black; background-color:gray")
		self.password.setStyleSheet("color: black; background-color:gray")

		self.iname.setStyleSheet("color: black; background-color:white")
		self.idob.setStyleSheet("color: black; background-color:white")
		self.igender.setStyleSheet("color: black; background-color:white")
		self.iphone.setStyleSheet("color: black; background-color:white")
		self.iaddress.setStyleSheet("color: black; background-color:white")
		self.ipassword.setStyleSheet("color: black; background-color:white")

		self.iname.setPlaceholderText('Enter Name')
		self.idob.setPlaceholderText('Enter Date of Birth')
		self.iphone.setPlaceholderText('Enter Phone NO.')
		self.iaddress.setPlaceholderText('Enter Address')
		self.ipassword.setPlaceholderText('Enter Password')


		self.name.setFixedWidth(150)
		self.iname.setFixedWidth(300)
		self.idob.setFixedWidth(300)
		self.igender.setFixedWidth(300)
		self.iphone.setFixedWidth(300)
		self.iaddress.setFixedWidth(300)
		self.ipassword.setFixedWidth(300)


		self.grid.addWidget(self.name,0,0)
		self.grid.addWidget(self.iname,0,1)
		self.grid.addWidget(self.dob,1,0)
		self.grid.addWidget(self.idob,1,1)
		self.grid.addWidget(self.gender,2,0)
		self.grid.addWidget(self.igender,2,1)
		self.grid.addWidget(self.phone,3,0)
		self.grid.addWidget(self.iphone,3,1)
		self.grid.addWidget(self.address,4,0)
		self.grid.addWidget(self.iaddress,4,1)
		self.grid.addWidget(self.password,5,0)
		self.grid.addWidget(self.ipassword,5,1)


		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Register")

		self.ok.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox_xy.addStretch(1)
		self.hbox_xy.addWidget(self.reset)
		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addStretch(1)

		self.main_layout = QVBoxLayout()
		self.main_layout.addStretch(1)
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.main_layout.addStretch(1)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(self.main_layout)

		self.connect(self.idob,SIGNAL("clicked()"),self.add_dob)
		self.connect(self.reset,SIGNAL("clicked()"),self.delete_all)
		self.connect(self.ok,SIGNAL("clicked()"),self.buttonEvent)

	def delete_all(self):
		self.iname.clear()
		self.ipassword.clear()
		self.iaddress.clear()
		self.iphone.clear()
		self.idob.clear()


	def add_dob(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)

	def date_changed(self,date):
		date = date.toPyDate()
		self.idob.setText(str(date))
		self.cal.close()


	def buttonEvent(self):
		name = str(self.iname.text())
		addr = str(self.iaddress.text())
		phone = str(self.iphone.text())
		dob = str(self.idob.text())
		gender = str(self.igender.currentText())
		password = str(self.ipassword.text())

		if(phone.isdigit() and len(phone)==10):
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor = db.cursor()
			try:
				cursor.execute("insert into Person(DOB,Name,Gender,Address,Phone) values('%s','%s','%s','%s','%s')"%(dob,name,gender,addr,phone))
				cursor.execute("select LAST_INSERT_ID()")
				pid = cursor.fetchone()[0]
				cursor.execute("insert into Patient(PID,Password) values('%d','%s')"%(pid,password))
				cursor.execute("select LAST_INSERT_ID()")
				patid = cursor.fetchone()[0]
				db.commit()
				message = QMessageBox(QMessageBox.Information,"Register Message","Registration Successful.Your Username is "+str(pid)+",Password is "+password+" and your Patient ID is "+str(patid),buttons = QMessageBox.Ok)
				message.exec_()
				self.delete_all()
			except Exception as e:
				print(e)
				db.rollback()
				message = QMessageBox(QMessageBox.Warning,"Register Message","Registration Failed. Try Again",buttons = QMessageBox.Close)
				message.exec_()
			db.close()
		else:
			message = QMessageBox(QMessageBox.Warning,"Register Message","Invalid Entry.",buttons = QMessageBox.Close)
			message.exec_()


	def showIT(self):
		self.delete_all()
		self.show()


