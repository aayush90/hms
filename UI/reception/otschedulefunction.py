import MySQLdb
import datetime


def getname(DID):
    db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
    cursor=db.cursor()
    cursor.execute("select Name from Person where PID in(select PID from Doctor where DID='%d')"%(DID))
    num=cursor.fetchone()[0]
    db.close()
    return num

# date is python datetime object, passed as parameter. It is required date at which operation is to be carried out.
# isEmergency=1 denotes operation MUST be done at same date.
# returns dictionary containing DID of doctor who operates(int), start time(string) , RoomNo and Building.

def OTschedule(date,PatID,FID,isEmergency):
    db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
    cursor=db.cursor()
    rdic={}
    dt=date.isoformat().split('T')[0]
    dy=date.strftime("%A")
    cursor.execute("select DID from Doctor where Qualification='MBBS MS' and Department in(select Department from Facility where FID='%d') and DID in (select DID from DoctorSchedule where Day='%s')"%(FID,dy))
    tl=cursor.fetchall()
    tl=list(tl)
    cursor.execute("select RoomNo,Building from LocationFacility where FID='%d'"%(FID))
    rl=cursor.fetchall()
    rl=list(rl)

    if len(tl) != 0:
        for i in tl:
            cursor.execute("select count(*) from LeaveSchedule where '%s' between StartTime and EndTime and PID in(select PID from Doctor where DID='%d')"%(dt+" 00:00:00",i[0]))
            numx=cursor.fetchone()[0]
            if numx>=1:
                continue
            for j in rl:
                cursor.execute("select max(EndTime) from OTschedule where (DID='%d' or (RoomNo='%s' and Building='%s')) and StartTime like '%s'"%(i[0],j[0],j[1],dt+str('%')))
                rs=cursor.fetchone()
                if rs is None or rs[0] is None:
                    sql="""insert into OTschedule(DID,PatID,StartTime,EndTime,FID,RoomNo,Building)
                        values('%d','%d','%s','%s','%d','%s','%s')"""%\
                        (i[0],PatID,dt+" 09:00:00",dt+" 11:00:00",FID,j[0],j[1])
                    cursor.execute(sql)
                    sql="""insert into OperationInfo(DID,FID,PatID,DOoper)
                    values('%d','%d','%d','%s')"""%\
                        (i[0],FID,PatID,dt)
                    cursor.execute(sql)
                    rdic['StartTime']=dt+" 09:00:00"
                    rdic['DName']=getname(i[0])
                    rdic['RoomNo']=j[0]
                    rdic['Building']=j[1]
                    db.commit()
                    db.close()
                    return rdic
                elif rs[0].hour<=21 and rs[0].hour>=9:
                    rdt=rs[0]+datetime.timedelta(hours=2)
                    rdt=rdt.isoformat().replace('T',' ')
                    sql="""insert into OTschedule(DID,PatID,StartTime,EndTime,FID,RoomNo,Building)
                        values('%d','%d','%s','%s','%d','%s','%s')"""%\
                        (i[0],PatID,str(rs[0]),rdt,FID,j[0],j[1])
                    cursor.execute(sql)
                    sql="""insert into OperationInfo(DID,FID,PatID,DOoper)
                    values('%d','%d','%d','%s')"""%\
                        (i[0],FID,PatID,dt)
                    cursor.execute(sql)
                    rdic['StartTime']=str(rs[0])
                    rdic['DName']=getname(i[0])
                    rdic['RoomNo']=j[0]
                    rdic['Building']=j[1]
                    db.commit()
                    db.close()
                    return rdic

    if isEmergency==1:
        cursor.execute("select DID from Doctor where Qualification='MBBS MS' and Type='visiting' and Department in(select Department from Facility where FID='%d') "%(FID))
        did=cursor.fetchone()[0]
        for j in rl:
            print dt+str('%')
            cursor.execute("select max(EndTime) from OTschedule where (DID='%d' or (RoomNo='%s' and Building='%s')) and StartTime like '%s' " % (did,j[0],j[1],dt+str('%')))
            rs=cursor.fetchone()
            if rs is None:
                rs=dt+" 09:00:00"
                re=dt+" 11:00:00"
            else:
                re=rs[0]+datetime.timedelta(hours=2)
                rs=str(rs[0])
                re=re.isoformat().replace('T',' ')

            sql="""insert into OTschedule(DID,PatID,StartTime,EndTime,FID,RoomNo,Building)
                    values('%d','%d','%s','%s','%d','%s','%s')"""%\
                    (did,PatID,rs,re,FID,j[0],j[1])
            cursor.execute(sql)
            sql="""insert into OperationInfo(DID,FID,PatID,DOoper)
                    values('%d','%d','%d','%s')"""%\
                    (did,FID,PatID,dt)
            cursor.execute(sql)
            rdic['StartTime']=rs
            rdic['DName']=getname(did)
            rdic['RoomNo']=j[0]
            rdic['Building']=j[1]
            db.commit()
            db.close()
            return rdic

    cursor.execute("select DID from Doctor where Qualification='MBBS MS' and Type<>'visiting' and Department in(select Department from Facility where FID='%d')"%(FID))
    tl=cursor.fetchall()
    tl=list(tl)
    while(1):
        dt=date.isoformat().split('T')[0]
        dy=date.strftime("%A")
        for i in tl:
            cursor.execute("select count(*) from LeaveSchedule where '%s' between StartTime and EndTime and PID in(select PID from Doctor where DID='%d')"%(dt+" 00:00:00",i[0]))
            numx=cursor.fetchone()[0]
            if numx>=1:
                continue
            cursor.execute("select count(*) from DoctorSchedule where DID='%d' and Day='%s'"%(i[0],dy))
            rs12=cursor.fetchone()[0]
            if rs12==0:
                continue
            for j in rl:
                cursor.execute("select max(EndTime) from OTschedule where (DID='%d' or (RoomNo='%s' and Building='%s')) and StartTime like '%s'"%(i[0],j[0],j[1],dt+str('%')))
                rs=cursor.fetchone()
                if rs[0] is None:
                    sql="""insert into OTschedule(DID,PatID,StartTime,EndTime,FID,RoomNo,Building)
                        values('%d','%d','%s','%s','%d','%s','%s')"""%\
                        (i[0],PatID,dt+" 09:00:00",dt+" 11:00:00",FID,j[0],j[1])
                    cursor.execute(sql)
                    sql="""insert into OperationInfo(DID,FID,PatID,DOoper)
                    values('%d','%d','%d','%s')"""%\
                        (i[0],FID,PatID,dt)
                    cursor.execute(sql)
                    rdic['StartTime']=dt+" 09:00:00"
                    rdic['DName']=getname(i[0])
                    rdic['RoomNo']=j[0]
                    rdic['Building']=j[1]
                    db.commit()
                    db.close()
                    return rdic
                elif rs[0].hour<=21 and rs[0].hour>=9:
                    rdt=rs[0]+datetime.timedelta(hours=2)
                    rdt=rdt.isoformat().replace('T',' ')
                    sql="""insert into OTschedule(DID,PatID,StartTime,EndTime,FID,RoomNo,Building)
                        values('%d','%d','%s','%s','%d','%s','%s')"""%\
                        (i[0],PatID,str(rs[0]),rdt,FID,j[0],j[1])
                    cursor.execute(sql)
                    sql="""insert into OperationInfo(DID,FID,PatID,DOoper)
                    values('%d','%d','%d','%s')"""%\
                        (i[0],FID,PatID,dt)
                    cursor.execute(sql)
                    rdic['StartTime']=str(rs[0])
                    rdic['DName']=getname(i[0])
                    rdic['RoomNo']=j[0]
                    rdic['Building']=j[1]
                    db.commit()
                    db.close()
                    return rdic
        date=date+datetime.timedelta(days=1)
