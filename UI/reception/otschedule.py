from PyQt4.QtCore import *
from checkValid import *
from otschedulefunction import *
import MySQLdb


class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))


class OTSchedule(QFrame):

	def __init__(self):
		super(OTSchedule,self).__init__()
		self.initUI()


	def initUI(self):

		self.patId = QLabel("Patient Id")
		self.ipatId = QLineEdit()
		self.surgery = QLabel("Department")
		self.isurgery = QComboBox()
		self.surgery_detail = {}
		self.setSurgery()
		self.date = QLabel("Date of Operation")
		self.idate = extQLineEdit()
		self.room = 0
		self.mode = QLabel("Mode")
		self.imode = QComboBox()
		self.imode.addItem("Cash")
		self.imode.addItem("Cheque")
		self.amount = QLabel("Amount")
		self.iamount = extQLineEdit()
		self.iamount.setEnabled(False)
		self.emergency = QCheckBox("Emergency")


		self.ipatId.setPlaceholderText('Enter Patient ID')
		self.iamount.setPlaceholderText('Enter Amount(in Rs)')
		self.idate.setPlaceholderText('Select Date of Operation')

		self.emergency.setStyleSheet("color: black; background-color:white")

		self.patId.setStyleSheet("color: black; background-color:gray")
		self.surgery.setStyleSheet("color: black; background-color:gray")
		self.mode.setStyleSheet("color: black; background-color:gray")
		self.amount.setStyleSheet("color: black; background-color:gray")
		self.date.setStyleSheet("color: black; background-color:gray")

		self.patId.setAlignment(Qt.AlignCenter)
		self.surgery.setAlignment(Qt.AlignCenter)
		self.mode.setAlignment(Qt.AlignCenter)
		self.amount.setAlignment(Qt.AlignCenter)
		self.date.setAlignment(Qt.AlignCenter)

		self.ipatId.setStyleSheet("color: black; background-color:white")
		self.isurgery.setStyleSheet("color: black; background-color:white")
		self.imode.setStyleSheet("color: black; background-color:white")
		self.iamount.setStyleSheet("color: black; background-color:white")
		self.idate.setStyleSheet("color: black; background-color:white")


		self.patId.setFixedWidth(150)
		self.ipatId.setFixedWidth(300)
		self.isurgery.setFixedWidth(300)
		self.imode.setFixedWidth(300)
		self.iamount.setFixedWidth(300)
		self.idate.setFixedWidth(300)
		self.emergency.setFixedWidth(300)

		self.grid = QGridLayout()
		self.grid.addWidget(self.patId,0,0)
		self.grid.addWidget(self.ipatId,0,1)
		self.grid.addWidget(self.surgery,1,0)
		self.grid.addWidget(self.isurgery,1,1)
		self.grid.addWidget(self.date,2,0)
		self.grid.addWidget(self.idate,2,1)
		self.grid.addWidget(self.mode,3,0)
		self.grid.addWidget(self.imode,3,1)
		self.grid.addWidget(self.amount,4,0)
		self.grid.addWidget(self.iamount,4,1)
		self.grid.addWidget(self.emergency,5,1)



		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Enter")

		self.ok.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox_xy.addStretch(1)
		self.hbox_xy.addWidget(self.reset)
		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addStretch(1)

		self.main_layout = QVBoxLayout()
		self.main_layout.addStretch(1)
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.main_layout.addStretch(1)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(self.main_layout)

		self.connect(self.reset,SIGNAL("clicked()"),self.clear_all)
		self.connect(self.ok,SIGNAL("clicked()"),self.enter)
		self.connect(self.isurgery,SIGNAL("activated(QString)"),self.setCombo)
		self.connect(self.idate,SIGNAL("clicked()"),self.getDate)



	def setSurgery(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct Name,Department,Cost,FID from Facility where Purpose='operation'")
		rows = cursor.fetchall()
		db.close()
		for row in rows:
			self.isurgery.addItem(row[0])
			self.surgery_detail[row[0]] = (row[1],row[2],row[3])


	def setCombo(self,surgery):
		self.iamount.setText(str(self.surgery_detail[str(surgery)][1]))


	def getDate(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		maxdate = QDate(QDate.currentDate().year(),QDate.currentDate().month()+4,QDate.currentDate().day())
		self.cal.setDateRange(QDate.currentDate(),maxdate)
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)


	def date_changed(self,date):
		date = date.toPyDate()
		self.idate.setText(str(date))
		self.cal.close()


	def clear_all(self):
		self.emergency.setChecked(False)
		self.ipatId.clear()
		self.iamount.clear()
		self.idate.clear()



	def enter(self):
		patID = str(self.ipatId.text())
		mode = str(self.imode.currentText())
		amount = str(self.iamount.text())
		date = str(self.idate.currentText())
		surgery = str(self.isurgery.currentText())
		emergency = int(self.emergency.isChecked())
		f = '%Y-%m-%d %H:%M:%S'
		today = str(datetime.now().strftime(f))
		try:
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor = db.cursor()
			details = OTschedule(date,patID,self.surgery_detail[surgery][3],emergency)
			cursor.execute("insert into Accounts(DOpayment,Mode,Purpose,Amount) values('%s','%s','%s','%f')"%(today,mode,"operation",float(amount)))
			db.commit()
			message = QMessageBox(QMessageBox.Information,"Operation","Operation Schedule Entered successfully.Your Operation will be done by Dr.+"+details["DName"]+" on "+details["StartTime"]+" , room no is "+details["RoomNo"]+" in building "+details["Building"],buttons = QMessageBox.Ok)
			message.exec_()
			self.clear_all()
		except Exception as e:
			print(e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Operation","Operation Schedule Failed. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()


	def showIT(self):
		self.clear_all()
		self.show()

