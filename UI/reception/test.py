from PyQt4.QtGui import *
from PyQt4.QtCore import *
import MySQLdb


class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))


class Test(QFrame):

	def __init__(self):
		super(Test,self).__init__()
		self.initUI()

	def initUI(self):

		self.facility = QLabel("Facility")
		self.ifacility = QComboBox()
		self.setFacility()
		self.patId = QLabel("Patient Id")
		self.ipatId = QLineEdit()
		self.dot = QLabel("Date of Test")
		self.idot = extQLineEdit()
		self.report = QLabel("Report")
		self.ireport = QTextEdit()


		self.ipatId.setPlaceholderText('Enter Patient ID')
		self.idot.setPlaceholderText('Enter Date of Test')
		self.ireport.setPlainText("Enter Patient's Report")


		self.facility.setStyleSheet("color: black; background-color:gray")
		self.patId.setStyleSheet("color: black; background-color:gray")
		self.dot.setStyleSheet("color: black; background-color:gray")
		self.report.setStyleSheet("color: black; background-color:gray")

		self.facility.setAlignment(Qt.AlignCenter)
		self.patId.setAlignment(Qt.AlignCenter)
		self.dot.setAlignment(Qt.AlignCenter)
		self.report.setAlignment(Qt.AlignCenter)

		self.ifacility.setStyleSheet("color: black; background-color:white")
		self.ipatId.setStyleSheet("color: black; background-color:white")
		self.idot.setStyleSheet("color: black; background-color:white")
		self.ireport.setStyleSheet("color: black; background-color:white")

		self.patId.setFixedWidth(150)
		self.ipatId.setFixedWidth(300)
		self.ifacility.setFixedWidth(300)
		self.idot.setFixedWidth(300)
		self.ireport.setFixedWidth(300)

		self.grid = QGridLayout()
		self.grid.addWidget(self.patId,0,0)
		self.grid.addWidget(self.ipatId,0,1)
		self.grid.addWidget(self.facility,1,0)
		self.grid.addWidget(self.ifacility,1,1)
		self.grid.addWidget(self.dot,2,0)
		self.grid.addWidget(self.idot,2,1)
		self.grid.addWidget(self.report,3,0)
		self.grid.addWidget(self.ireport,3,1,3,1)




		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Enter")

		self.ok.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox_xy.addStretch(1)
		self.hbox_xy.addWidget(self.reset)
		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addStretch(1)

		self.main_layout = QVBoxLayout()
		self.main_layout.addStretch(1)
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.main_layout.addStretch(1)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(self.main_layout)

		self.connect(self.reset,SIGNAL("clicked()"),self.delete_all)
		self.connect(self.ok,SIGNAL("clicked()"),self.enterInfo)
		self.connect(self.idot,SIGNAL("clicked()"),self.setDOT)


	def setFacility(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct Name from Facility where Purpose='test'")
		rows = cursor.fetchall()
		for row in rows:
			self.ifacility.addItem(row[0])
		db.close()


	def setDOT(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)

	def date_changed(self,date):
		date = date.toPyDate()
		self.idot.setText(str(date))
		self.cal.close()


	def delete_all(self):
		self.idot.clear()
		self.ireport.clear()
		self.ipatId.clear()


	def enterInfo(self):
		fid = self.ifacility.currentIndex()
		patid = str(self.ipatId.text())
		report = str(self.ireport.toPlainText())
		dot = str(self.idot.text())

		try:
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor = db.cursor()
			cursor.execute("insert into TestInfo values('%d','%d','%s','%s')"%(fid,int(patid),dot,report))
			db.commit()
			message = QMessageBox(QMessageBox.Information,"Entry","Information Entered successfully.",buttons = QMessageBox.Ok)
			message.exec_()
			self.delete_all()
		except Exception as e:
			print(e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Entry","Information Entry Failed. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()


	def showIT(self):
		self.delete_all()
		self.show()

