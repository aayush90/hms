from PyQt4.QtGui import *
from PyQt4.QtCore import *
from datetime import datetime,timedelta
import MySQLdb

class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))


class Appointment(QFrame):

	def __init__(self):
		super(Appointment,self).__init__()
		self.initUI()


	def initUI(self):

		self.patId = QLabel("Patient Id")
		self.ipatId = QLineEdit()
		self.docname = QLabel("Doctor Name")
		self.docpid=[]
		self.idocname = QComboBox()
		self.dept = QLabel("Department")
		self.idept = QComboBox()
		self.emergency = QCheckBox("Emergency Case")
		self.setDept()
		self.mode = QLabel("Mode")
		self.imode = QComboBox()
		self.imode.addItem("Cash")
		self.imode.addItem("Cheque")
		self.imode.addItem("Card")
		self.amount = QLabel("Amount")
		self.iamount = extQLineEdit()
		self.current_docpid = 0


		self.ipatId.setPlaceholderText('Enter Patient ID')
		self.iamount.setPlaceholderText('Enter Amount(in Rs)')

		self.docname.setStyleSheet("color: black; background-color:gray")
		self.patId.setStyleSheet("color: black; background-color:gray")
		self.dept.setStyleSheet("color: black; background-color:gray")
		self.mode.setStyleSheet("color: black; background-color:gray")
		self.amount.setStyleSheet("color: black; background-color:gray")

		self.docname.setAlignment(Qt.AlignCenter)
		self.patId.setAlignment(Qt.AlignCenter)
		self.dept.setAlignment(Qt.AlignCenter)
		self.mode.setAlignment(Qt.AlignCenter)
		self.amount.setAlignment(Qt.AlignCenter)

		self.idocname.setStyleSheet("color: black; background-color:white")
		self.ipatId.setStyleSheet("color: black; background-color:white")
		self.idept.setStyleSheet("color: black; background-color:white")
		self.emergency.setStyleSheet("color: black; background-color:white")
		self.imode.setStyleSheet("color: black; background-color:white")
		self.iamount.setStyleSheet("color: black; background-color:white")


		self.patId.setFixedWidth(150)
		self.ipatId.setFixedWidth(300)
		self.idocname.setFixedWidth(300)
		self.idept.setFixedWidth(300)
		self.emergency.setFixedWidth(300)
		self.imode.setFixedWidth(300)
		self.iamount.setFixedWidth(300)

		self.grid = QGridLayout()
		self.grid.addWidget(self.patId,0,0)
		self.grid.addWidget(self.ipatId,0,1)
		self.grid.addWidget(self.dept,1,0)
		self.grid.addWidget(self.idept,1,1)
		self.grid.addWidget(self.docname,2,0)
		self.grid.addWidget(self.idocname,2,1)
		self.grid.addWidget(self.mode,3,0)
		self.grid.addWidget(self.imode,3,1)
		self.grid.addWidget(self.amount,4,0)
		self.grid.addWidget(self.iamount,4,1)
		self.grid.addWidget(self.emergency,5,1)



		self.hbox_xy = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.ok = QPushButton("Enter")

		self.ok.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox_xy.addStretch(1)
		self.hbox_xy.addWidget(self.reset)
		self.hbox_xy.addWidget(self.ok)
		self.hbox_xy.addStretch(1)

		self.main_layout = QVBoxLayout()
		self.main_layout.addStretch(1)
		self.main_layout.addLayout(self.grid)
		self.main_layout.addLayout(self.hbox_xy)
		self.main_layout.addStretch(1)

		self.setStyleSheet("background-color:lightblue;")
		self.setLayout(self.main_layout)

		self.connect(self.reset,SIGNAL("clicked()"),self.clear_all)
		self.connect(self.ok,SIGNAL("clicked()"),self.enter)
		self.connect(self.idept,SIGNAL("activated(QString)"),self.setDoctor)
		self.connect(self.idocname,SIGNAL("activated(int)"),self.setPid)



	def setDept(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct Department from Doctor")
		rows = cursor.fetchall()
		db.close()
		for row in rows:
			self.idept.addItem(row[0])


	def setDoctor(self,dept):
		self.idocname.clear()
		del self.docpid[:]
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select distinct PID,DID from Doctor where Department='%s'"%(str(dept)))
		rows = cursor.fetchall()
		for row in rows:
			cursor.execute("select Name from Person where PID='%d'"%(row[0]))
			name = cursor.fetchone()
			self.idocname.addItem(name[0])
			self.docpid.append(row[1])
		db.close()


	def setPid(self,index):
		self.current_docpid = self.docpid[index]


	def clear_all(self):
		self.ipatId.clear()
		self.iamount.clear()
		self.idocname.clear()



	def enter(self):
		patid = str(self.ipatId.text())
		emergency = int(self.emergency.isChecked())
		mode = str(self.imode.currentText())
		amount = str(self.iamount.text())
		f = '%Y-%m-%d %H:%M:%S'
		dt = datetime.now().strftime(f)
		at = datetime.now().strftime(f)

		try:
			db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cursor = db.cursor()
			cursor.execute("select count(*) from LeaveSchedule where '%s' between StartTime and EndTime and PID in(select PID from Doctor where DID='%d')"%(dt,self.current_docpid))
			numx=cursor.fetchone()[0]
			if numx>=1:
				raise Exception("Doctor on leave")
			cursor.execute("select max(AppointmentTime) from PatientEntry where PatId='%d' and DID='%d' and AppointmentTime > '%s'"%(int(patid),self.current_docpid,dt))
			rows = cursor.fetchall()
			if(rows[0][0] is None):
				at = (datetime.now()+timedelta(minutes=15)).strftime(f)
			else:
				at = (datetime.strptime(rows[0][0],f)+timedelta(minutes=20)).strftime(f)
			cursor.execute("insert into PatientEntry values('%d','%d','%d','%d','%s','%s')"%(int(patid),self.current_docpid,emergency,0,dt,at))
			cursor.execute("insert into Accounts(DOpayment,Mode,Purpose,Amount) values('%s','%s','%s','%f')"%(dt,mode,"entry",float(amount)))
			db.commit()
			message = QMessageBox(QMessageBox.Information,"Patient Entry","Patient Entered successfully.Your appointment time is "+at,buttons = QMessageBox.Ok)
			message.exec_()
			self.clear_all()
		except Exception as e:
			print(e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Patient Entry","Patient Entry Failed. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()


	def showIT(self):
		self.clear_all()
		self.show()
