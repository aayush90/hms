from patientregister import *
from appointment import *
from test import *
from admit import *
from otschedule import *
from UI.ApplyLeave import *
import MySQLdb


class Reception(QWidget):

	def __init__(self,parent):
		super(Reception,self).__init__()
		self.parent = parent
		self.pid = 0
		self.register = PatientRegister()
		self.appointment = Appointment()
		self.testinfo = Test()
		self.otschedule = OTSchedule()
		self.admit = Admit()
		self.leave = Leave()
		self.initUI()

	def initUI(self):
		self.setWindowTitle("Reception HomePage")
		self.setWindowIcon(QIcon('/home/aayush/Desktop/assign/dbms/HMS/sample3.jpg')) 	# App Icon
		self.name = QLabel()
		self.leavebtn = QPushButton("Apply for Leave")
		self.registerbtn = QPushButton("Register New Patient")
		self.appointbtn = QPushButton("Give Appointment")
		self.testbtn = QPushButton("Insert Test Information")
		self.admitbtn = QPushButton("Admit Patient")
		self.otschedulebtn = QPushButton("Operation Schedule")
		self.logout = QPushButton("LogOut")
		self.temp = QTextEdit()
		self.temp.setEnabled(False)
		self.temp.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)

		self.name.setStyleSheet("color:black;")
		self.leavebtn.setStyleSheet("color: black; background-color:gray")
		self.registerbtn.setStyleSheet("color: black; background-color:gray")
		self.appointbtn.setStyleSheet("color: black; background-color:gray")
		self.testbtn.setStyleSheet("color: black; background-color:gray")
		self.admitbtn.setStyleSheet("color: black; background-color:gray")
		self.otschedulebtn.setStyleSheet("color: black; background-color:gray")
		self.logout.setStyleSheet("color: black; background-color:red")

		self.grid = QGridLayout()
		self.grid.addWidget(self.name,0,10,1,1)
		self.grid.addWidget(self.logout,0,11,1,1)
		self.grid.addWidget(self.leavebtn,2,0,1,1)
		self.grid.addWidget(self.registerbtn,3,0,1,1)
		self.grid.addWidget(self.appointbtn,4,0,1,1)
		self.grid.addWidget(self.testbtn,5,0,1,1)
		self.grid.addWidget(self.admitbtn,6,0,1,1)
		self.grid.addWidget(self.otschedulebtn,7,0,1,1)
		self.grid.addWidget(self.temp,1,3,8,9)

		self.setLayout(self.grid)
		self.setStyleSheet("background-color:white")

		self.connect(self.logout,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.registerbtn,SIGNAL("clicked()"),self.addRegister)
		self.connect(self.appointbtn,SIGNAL("clicked()"),self.giveAppointment)
		self.connect(self.testbtn,SIGNAL("clicked()"),self.insertTestInfo)
		self.connect(self.admitbtn,SIGNAL("clicked()"),self.insertAdmitInfo)
		self.connect(self.otschedulebtn,SIGNAL("clicked()"),self.giveOTSchedule)
		self.connect(self.leavebtn,SIGNAL("clicked()"),self.applyLeave)



	def buttonEvent(self):
		self.close()
		self.parent.showMaximized()


	def giveAppointment(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.register.close()
		self.testinfo.close()
		self.admit.close()
		self.otschedule.close()
		self.leave.close()
		self.grid.addWidget(self.appointment,1,3,8,9)
		self.appointment.showIT()


	def addRegister(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.appointment.close()
		self.testinfo.close()
		self.admit.close()
		self.otschedule.close()
		self.leave.close()
		self.grid.addWidget(self.register,1,3,8,9)
		self.register.showIT()


	def insertTestInfo(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.appointment.close()
		self.register.close()
		self.admit.close()
		self.otschedule.close()
		self.leave.close()
		self.grid.addWidget(self.testinfo,1,3,8,9)
		self.testinfo.showIT()


	def insertAdmitInfo(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.appointment.close()
		self.register.close()
		self.testinfo.close()
		self.otschedule.close()
		self.leave.close()
		self.grid.addWidget(self.admit,1,3,8,9)
		self.admit.showIT()


	def giveOTSchedule(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.appointment.close()
		self.register.close()
		self.testinfo.close()
		self.admit.close()
		self.leave.close()
		self.grid.addWidget(self.otschedule,1,3,8,9)
		self.otschedule.showIT()


	def applyLeave(self):
		self.grid.itemAtPosition(1,3).widget().close()
		self.appointment.close()
		self.register.close()
		self.testinfo.close()
		self.admit.close()
		self.otschedule.close()
		self.grid.addWidget(self.leave,1,3,8,9)
		self.leave.showIT(self.pid)

	def findPatientHistory(self):
		pass


	def showIT(self,pid):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor = db.cursor()
		cursor.execute("select Name from Person where PID='%s'"%(pid))
		rows = cursor.fetchall()
		db.close()
		self.pid = rows[0][0]
		self.name.setText(rows[0][0])
		self.showMaximized()


	def checkSchedule(self):
		pass