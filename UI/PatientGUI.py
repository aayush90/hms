from PyQt4.QtGui import *
from PyQt4.QtCore import *
from patient import *


class extQLineEdit(QLineEdit):
	def __init__(self):
		QLineEdit.__init__(self)
	def mousePressEvent(self,QMouseEvent):
		self.emit(SIGNAL("clicked()"))

class PatientGUI(QFrame):
	def __init__(self,parent):
		super(PatientGUI, self).__init__()
		self.parent = parent
		self.initAction()
		self.init_layout()



	def initAction(self):
		self.setWindowIcon(QIcon('/home/aayush/Desktop/assign/dbms/HMS/sample3.jpg')) 	# App Icon

		self.nameLabel = QLabel('Name:')
		self.nameLabel.setAlignment(Qt.AlignCenter)
		self.dobLabel = QLabel('DOB:')
		self.dobLabel.setAlignment(Qt.AlignCenter)
		self.addrLabel = QLabel('Address:')
		self.addrLabel.setAlignment(Qt.AlignCenter)
		self.genderLabel = QLabel('Gender:')
		self.genderLabel.setAlignment(Qt.AlignCenter)
		self.phoneLabel = QLabel('Phone:')
		self.phoneLabel.setAlignment(Qt.AlignCenter)
		self.passLabel = QLabel('Password')

		# self.nameLabel.setFixedWidth(50)

		self.nameLabel.setStyleSheet("color: black;background-color:gray")
		self.dobLabel.setStyleSheet("color: black;background-color:gray")
		self.addrLabel.setStyleSheet("color: black;background-color:gray")
		self.genderLabel.setStyleSheet("color: black;background-color:gray")
		self.phoneLabel.setStyleSheet("color: black;background-color:gray")
		self.passLabel.setStyleSheet("color: black;background-color:gray")

		self.nameText = QLineEdit()
		self.nameText.setEnabled(False)
		self.dobText = extQLineEdit()
		self.dobText.setEnabled(False)
		self.connect(self.dobText,SIGNAL('clicked()'),self.setDate)
		self.genderText = QLineEdit()
		self.genderText.setEnabled(False)
		self.addrText = QLineEdit()
		self.addrText.setEnabled(False)
		self.phoneText = QLineEdit()
		self.phoneText.setEnabled(False)
		self.passText = QLineEdit()
		self.passText.setEnabled(False)

		self.nameText.setStyleSheet("color: black;background-color:white")
		self.dobText.setStyleSheet("color: black;background-color:white")
		self.genderText.setStyleSheet("color: black;background-color:white")
		self.addrText.setStyleSheet("color: black;background-color:white")
		self.phoneText.setStyleSheet("color: black;background-color:white")
		self.passText.setStyleSheet("color: black;background-color:white")


		self.transButton = QPushButton('My Transactions')
		self.connect(self.transButton,SIGNAL('clicked()'),self.viewTransactions)
		self.checkupButton = QPushButton('My CheckUps')
		self.connect(self.checkupButton,SIGNAL('clicked()'),self.viewCheckUp)
		self.appointButton = QPushButton('My Appointments')
		self.connect(self.appointButton,SIGNAL('clicked()'),self.viewAppointments)

		self.transButton.setStyleSheet("color: black;background-color:lightgray")
		self.checkupButton.setStyleSheet("color: black;background-color:lightgray")
		self.appointButton.setStyleSheet("color: black;background-color:lightgray")

		# columns = ['ItemID', 'Name', 'Qty', 'Unit Price', 'Amount']
		self.table = QTableWidget()
		self.table.setStyleSheet("color: black;background-color:white")
		self.table.setColumnCount(1)
		# self.table.setHorizontalHeaderLabels(columns)
		# self.table.setSortingEnabled(True)
		self.table.horizontalHeader().setResizeMode(QHeaderView.Stretch)
		self.table.resizeColumnsToContents()

		self.printButton = QPushButton('Done')
		self.editButton = QPushButton("Edit")
		self.updateButton = QPushButton("Update")
		self.updateButton.setEnabled(False)
		self.logoutButton = QPushButton("Close")
		self.connect(self.printButton,SIGNAL('clicked()'),self.saveDetails)
		self.connect(self.editButton,SIGNAL('clicked()'),self.editDetails)
		self.connect(self.updateButton,SIGNAL('clicked()'),self.updateDetails)
		self.connect(self.logoutButton,SIGNAL('clicked()'),self.logout)

		self.printButton.setFixedWidth(200)
		self.editButton.setFixedWidth(200)
		self.updateButton.setFixedWidth(200)
		self.logoutButton.setFixedWidth(200)

		self.printButton.setStyleSheet("color: black;background-color:lightgray")
		self.editButton.setStyleSheet("color: black;background-color:lightgray")
		self.updateButton.setStyleSheet("color: black;background-color:lightgray")
		self.logoutButton.setStyleSheet("color: black;background-color:lightgray")

	def init_layout(self):
		self.setWindowTitle("Patient Information")
		self.p_layout = QGridLayout(self)
		temp = QLabel()
		temp.setFixedHeight(20)
		self.p_layout.addWidget(temp,0,0,1,-1)

		temp1 = QLabel()
		temp1.setFixedHeight(15)
		self.p_layout.addWidget(temp1,1,0,1,-1)

		self.p_layout.addWidget(self.nameLabel,2,0,1,1)
		self.p_layout.addWidget(self.nameText,2,1,1,1)
		self.p_layout.addWidget(self.dobLabel,2,2,1,1)
		self.p_layout.addWidget(self.dobText,2,3,1,1)
		self.p_layout.addWidget(self.genderLabel,2,4,1,1)
		self.p_layout.addWidget(self.genderText,2,5,1,1)

		self.p_layout.addWidget(self.addrLabel,3,0,1,1)
		self.p_layout.addWidget(self.addrText,3,1,1,3)
		self.p_layout.addWidget(self.phoneLabel,3,4,1,1)
		self.p_layout.addWidget(self.phoneText,3,5,1,1)



		temp2 = QLabel()
		temp2.setFixedHeight(15)
		self.p_layout.addWidget(temp2,4,0,1,-1)
		self.p_layout.addWidget(self.transButton,5,0,1,2)
		self.p_layout.addWidget(self.checkupButton,5,2,1,2)
		self.p_layout.addWidget(self.appointButton,5,4,1,2)
		self.p_layout.addWidget(self.table,6,0,4,-1)
		self.p_layout.addWidget(self.editButton,10,2,1,1)
		self.p_layout.addWidget(self.updateButton,10,3,1,1)
		# self.p_layout.addWidget(self.printButton,10,4,1,1)
		self.p_layout.addWidget(self.logoutButton,10,5,1,1)


		self.setStyleSheet("background-color:lightblue;")



	def setDate(self):
		self.cal = QCalendarWidget(self)
		self.cal.setGridVisible(True)
		self.cal.setStyleSheet("background-color:lightyellow;color:black")
		self.cal.move(300,300)
		self.cal.resize(350,300)
		self.cal.show()
		self.connect(self.cal,SIGNAL("clicked(QDate)"),self.date_changed)

	def date_changed(self,date):
		date = date.toPyDate()
		self.dobText.setText(str(date))
		self.cal.close()


	def viewTransactions(self):
		self.table.clear()
		self.table.setColumnCount(4)
		columns = ['Date', 'Purpose','Mode','Amount']
		self.table.setHorizontalHeaderLabels(columns)
		self.transDataDic = self.me.getTransactionDetails()
		l = len(self.transDataDic)
		self.table.setRowCount(l+2)
		total = 0.0
		for i in range(l):
			for j in range(4):
				it = QTableWidgetItem()
				it.setFlags(it.flags() & ~Qt.ItemIsEditable)
				text = str(self.transDataDic[i][columns[j]])
				it.setText(text)
				self.table.setItem(i,j,it)
			total += self.transDataDic['Amount']

		it = QTableWidgetItem()
		it.setFlags(it.flags() & ~Qt.ItemIsEditable)
		text = str(total)
		it.setText(text)
		self.table.setItem(l-1,3,it)

		itTemp = QTableWidgetItem()
		itTemp.setFlags(it.flags() & ~Qt.ItemIsEditable)
		text = 'Total'
		itTemp.setText(text)
		self.table.setItem(l-1,2,itTemp)

	def viewCheckUp(self):
		self.table.clear()
		self.table.setColumnCount(6)
		columns = ['DoctorName', 'Qualification','Department','CheckUpTime','Diagnosis','Treatment']
		self.table.setHorizontalHeaderLabels(columns)
		self.checkUpDataDic = self.me.getCheckUpInfo()
		l = len(self.checkUpDataDic)
		self.table.setRowCount(l)
		for i in range(l):
			for j in range(6):
				it = QTableWidgetItem()
				it.setFlags(it.flags() & ~Qt.ItemIsEditable)
				text = str(self.checkUpDataDic[i][columns[j]])
				it.setText(text)
				self.table.setItem(i,j,it)

	def viewAppointments(self):
		self.table.clear()
		self.table.setColumnCount(5)
		columns = ['DoctorName', 'Qualification','Department','AppointmentTime','IsHomeVisit']
		self.table.setHorizontalHeaderLabels(columns)
		self.appointmentDataDic = self.me.viewAppointment()
		l = len(self.appointmentDataDic)
		self.table.setRowCount(l)
		for i in range(l):
			for j in range(5):
				it = QTableWidgetItem()
				it.setFlags(it.flags() & ~Qt.ItemIsEditable)
				text = str(self.appointmentDataDic[i][columns[j]])
				it.setText(text)
				self.table.setItem(i,j,it)


	def showMyDetails(self):
		self.nameText.setEnabled(False)
		self.phoneText.setEnabled(False)
		self.addrText.setEnabled(False)
		self.myDetails = self.me.getPersonalInfo()
		self.nameText.setText(self.myDetails['Name'])
		self.dobText.setText(self.myDetails['DOB'])
		self.genderText.setText(self.myDetails['Gender'])
		self.addrText.setText(self.myDetails['Address'])
		self.phoneText.setText(self.myDetails['Phone'])
		self.passText.setText(self.myDetails['Password'])


	def editDetails(self):
		self.nameText.setEnabled(True)
		self.phoneText.setEnabled(True)
		self.addrText.setEnabled(True)
		self.passText.setEnabled(True)
		self.updateButton.setEnabled(True)

	def saveDetails(self):
		pass

	def updateDetails(self):
		info = {}
		info["name"] = str(self.nameText.text())
		info["addr"] = str(self.dobText.text())
		info["phone"] = str(self.addrText.text())
		info["Password"] = str(self.passText.text())
		if(len(info["phone"])==10 and info["name"] and info["addr"] and info["Password"]):
			if(self.me.updateInfo(info)):
				message = QMessageBox(QMessageBox.Information,"Update Information Message","Information Successfully Updated.",buttons = QMessageBox.Ok)
				message.exec_()
			else:
				message = QMessageBox(QMessageBox.Warning,"Update Information Message","Information Update failed.Try Again",buttons = QMessageBox.Close)
				message.exec_()
		else:
			message = QMessageBox(QMessageBox.Warning,"Update Information Message","Invalid Entry.",buttons = QMessageBox.Close)
			message.exec_()


	def showIT(self,patid):
		self.me = Patient(patid)
		self.showMyDetails()
		self.showMaximized()


	def logout(self):
		self.close()
		self.parent.showMaximized()
		


