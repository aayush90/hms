__author__ = 'batman'

from PyQt4 import QtGui
from UI.pharmacy.DocPrescription import *
from UI.pharmacy.Modify import *
from UI.pharmacy.SelfPrescription import *
import MySQLdb


class Pharmacy(QtGui.QTabWidget):
	def __init__(self,parent):
		super(Pharmacy, self).__init__()
		self.parent = parent
		self.initUI()

	def initUI(self):
		self.setWindowTitle('Pharmacy')
		self.setWindowIcon(QtGui.QIcon('/home/aayush/Desktop/assign/dbms/HMS/sample3.jpg')) 	# App Icon
		self.updateTab = Modify()
		self.docTab = DocPrescription()
		self.patTab = SelfPrescription()
		self.addTab(self.docTab,'Doctor Prescribed')
		self.addTab(self.patTab,'Self Prescribed')
		self.addTab(self.updateTab,'Update Database')

		self.setStyleSheet("background-color:lightblue")


	def showIT(self):
		self.showMaximized()


	def closeEvent(self, event):
		self.close()
		self.parent.showMaximized()


# app = QtGui.QApplication(sys.argv)
# app.setStyle(QtGui.QStyleFactory.create('cleanlooks'))
# a= Pharmacy()
# a.show()
# app.exec_()
