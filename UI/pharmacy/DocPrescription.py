__author__ = 'batman'

from PyQt4 import QtGui,QtCore
import MySQLdb as mdb
import datetime

class DocPrescription(QtGui.QWidget):
	def __init__(self):
		super(DocPrescription, self).__init__()
		self.initAction()
		self.init_layout()
		self.flagForNew = False

	def initAction(self):
		self.searchBox = QtGui.QLineEdit()
		self.searchBox.setPlaceholderText('Enter Checkup ID')
		self.searchButton = QtGui.QPushButton('Get Bill')
		self.connect(self.searchButton,QtCore.SIGNAL('clicked()'),self.queryProcess)

		self.searchBox.setStyleSheet("color:black;background-color:white")
		self.searchButton.setStyleSheet("color:black;background-color:lightgray")

		self.nameLabel = QtGui.QLabel('Name:')
		self.nameLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignCenter)
		self.dobLabel = QtGui.QLabel('DOB:')
		self.dobLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignCenter)
		self.addrLabel = QtGui.QLabel('Address:')
		self.addrLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignCenter)
		self.genderLabel = QtGui.QLabel('Gender:')
		self.genderLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignCenter)
		self.phoneLabel = QtGui.QLabel('Phone:')
		self.phoneLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignCenter)

		self.nameLabel.setStyleSheet("color:black;background-color:gray")
		self.dobLabel.setStyleSheet("color:black;background-color:gray")
		self.addrLabel.setStyleSheet("color:black;background-color:gray")
		self.genderLabel.setStyleSheet("color:black;background-color:gray")
		self.phoneLabel.setStyleSheet("color:black;background-color:gray")


		self.nameText = QtGui.QLineEdit()
		self.dobText = QtGui.QLineEdit()
		self.genderText = QtGui.QLineEdit()
		self.addrText = QtGui.QLineEdit()
		self.phoneText = QtGui.QLineEdit()

		self.nameText.setStyleSheet("color:black;background-color:white")
		self.dobText.setStyleSheet("color:black;background-color:white")
		self.addrText.setStyleSheet("color:black;background-color:white")
		self.genderText.setStyleSheet("color:black;background-color:white")
		self.phoneText.setStyleSheet("color:black;background-color:white")

		columns = ['ItemID', 'Name', 'Qty', 'Unit Price', 'Amount']
		self.bill = QtGui.QTableWidget()
		self.bill.setStyleSheet("color:black;background-color:white")
		self.bill.setColumnCount(5)
		self.bill.setHorizontalHeaderLabels(columns)
		self.bill.setSortingEnabled(True)
		self.bill.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.bill.resizeColumnsToContents()

		self.modeCombo = QtGui.QComboBox()
		self.modeCombo.setStyleSheet("color:black;background-color:lightgray")
		self.modeCombo.addItems(['Cash','Card','Cheque','Other'])

		self.printButton = QtGui.QPushButton('Complete Transaction')
		self.printButton.setStyleSheet("color:black;background-color:lightgray")
		self.connect(self.printButton,QtCore.SIGNAL('clicked()'),self.printBill)

	def init_layout(self):
		self.p_layout = QtGui.QGridLayout(self)
		temp = QtGui.QLabel()
		temp.setFixedHeight(20)
		self.p_layout.addWidget(temp,0,0,1,-1)
		self.p_layout.addWidget(self.searchBox,1,0,1,4)
		self.p_layout.addWidget(self.searchButton,1,4,1,2)

		temp1 = QtGui.QLabel()
		temp1.setFixedHeight(15)

		self.p_layout.addWidget(temp1,2,0,1,-1)

		self.p_layout.addWidget(self.nameLabel,3,0,1,1)
		self.p_layout.addWidget(self.nameText,3,1,1,1)
		self.p_layout.addWidget(self.dobLabel,3,2,1,1)
		self.p_layout.addWidget(self.dobText,3,3,1,1)
		self.p_layout.addWidget(self.genderLabel,3,4,1,1)
		self.p_layout.addWidget(self.genderText,3,5,1,1)

		self.p_layout.addWidget(self.addrLabel,4,0,1,1)
		self.p_layout.addWidget(self.addrText,4,1,1,3)
		self.p_layout.addWidget(self.phoneLabel,4,4,1,1)
		self.p_layout.addWidget(self.phoneText,4,5,1,1)

		temp2 = QtGui.QLabel()
		temp2.setFixedHeight(15)
		self.p_layout.addWidget(temp2,5,0,1,-1)
		self.p_layout.addWidget(self.bill,6,0,4,-1)
		self.p_layout.addWidget(self.modeCombo,10,4,1,1)
		self.p_layout.addWidget(self.printButton,10,5,1,1)

	def queryProcess(self):
		self.query = str(self.searchBox.text())
		if self.query != '':
			self.cid = int(self.query)
			try:
				db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
				cur = db.cursor()
				cur.execute('SELECT Stock.ItemID,Stock.Name,Stock.AvailableQty,Stock.UnitPrice,GiveMedicines.Quantity FROM Stock NATURAL JOIN GiveMedicines WHERE CID = %s',(self.cid))
				self.items = cur.fetchall()
				cur.execute('SELECT * FROM Person WHERE PID in (SELECT PID FROM Patient WHERE PatID in (SELECT PatID FROM CheckUp WHERE CID = %s))',(self.cid))
				self.patient = cur.fetchall()[0]
				cur.execute('SELECT PatID FROM CheckUp WHERE CID = %s',(self.cid))
				self.patId = cur.fetchall()[0][0]

				db.close()
				self.patientDetails()
				self.viewInTable()
			except Exception, e:
				msg = QtGui.QMessageBox()
				msg.setText('Error Occured in query processing!\n'+repr(e)+'\nTry Again.')
				msg.setIcon(QtGui.QMessageBox.Critical)
				msg.exec_()
				print repr(e)

	def viewInTable(self):
		self.bill.clear()
		columns = ['ItemID', 'Name', 'Qty', 'Unit Price', 'Amount']
		self.bill.setHorizontalHeaderLabels(columns)
		numItem = len(self.items)
		self.bill.setRowCount(numItem)
		self.amount = 0.0;
		for i in range(numItem):
			temp = min(self.items[i][2],self.items[i][4])
			for j in range(4):
				it = QtGui.QTableWidgetItem()
				it.setFlags(it.flags() & ~QtCore.Qt.ItemIsEditable)
				text = str(self.items[i][j])if j!=2 else str(temp)
				it.setText(text)
				self.bill.setItem(i,j,it)

			it = QtGui.QTableWidgetItem()
			it.setFlags(it.flags() & ~QtCore.Qt.ItemIsEditable)
			it.setText(str(temp*self.items[i][3]))
			self.bill.setItem(i,4,it)
			self.amount += temp*self.items[i][3]

		self.bill.insertRow(self.bill.rowCount())
		self.bill.insertRow(self.bill.rowCount())
		amountItem = QtGui.QTableWidgetItem()
		amountItem.setFlags(amountItem.flags() & ~QtCore.Qt.ItemIsEditable)
		amountItem.setText(str(self.amount))
		self.bill.setItem(self.bill.rowCount()-1,4,amountItem)
		totalItem = QtGui.QTableWidgetItem()
		totalItem.setFlags(totalItem.flags() & ~QtCore.Qt.ItemIsEditable)
		totalItem.setText('Total')
		self.bill.setItem(self.bill.rowCount()-1,3,totalItem)
		self.flagForNew = True

	def printBill(self):
		if self.flagForNew:
			self.updateStock()
			mode = self.modeCombo.currentText()
			amount = self.amount
			self.updateAccounts(mode=mode,amount=amount,patid=self.patId)
			self.flagForNew = False
			msg = QtGui.QMessageBox()
			msg.setText('The transaction is successful.\nDatabase updated.')
			msg.setIcon(QtGui.QMessageBox.Information)
			msg.exec_()
		#Add Code to print bill in text format


	def patientDetails(self):
		self.nameText.setText(QtCore.QString(self.patient[2]))
		self.dobText.setText(QtCore.QString(str(self.patient[1])))
		self.genderText.setText(QtCore.QString(str(self.patient[3])))
		self.addrText.setText(QtCore.QString(str(self.patient[4])))
		self.phoneText.setText(QtCore.QString(str(self.patient[5])))

	def updateStock(self):
		total = self.bill.rowCount()
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()
			updateQuery = 'UPDATE Stock SET AvailableQty = AvailableQty-%s WHERE ItemID = %s'
			for i in range(total-2):
				try:
					cur.execute(updateQuery,(self.bill.item(i,2).text(),self.bill.item(i,0).text()))
				except Exception, e:
					print repr(e)
			db.commit()
			db.close()
		except Exception, e:
			db.rollback()
			db.close()
			msg = QtGui.QMessageBox()
			msg.setText('Error Occured in stock updation!\nTry Again.')
			msg.setIcon(QtGui.QMessageBox.Critical)
			msg.exec_()
			print repr(e)

	def updateAccounts(self,mode='cash',date=datetime.datetime.now().strftime('%Y-%m-%d'),purpose='store',amount=0.0,patid=None):
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()
			cur.execute('INSERT INTO Accounts (DOPayment,Mode,Purpose,Amount,PatID) VALUES (%s,%s,%s,%s,%s)',(date,mode,purpose,amount,patid))
			db.commit()
			db.close()
		except Exception, e:
			db.rollback()
			db.close()
			msg = QtGui.QMessageBox()
			msg.setText('Error Occured in account updation!\nTry Again.')
			msg.setIcon(QtGui.QMessageBox.Critical)
			msg.exec_()
			print repr(e)
