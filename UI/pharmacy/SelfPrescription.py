__author__ = 'batman'

from PyQt4 import QtGui,QtCore
import MySQLdb as mdb
import datetime

class SelfPrescription(QtGui.QWidget):
	def __init__(self):
		super(SelfPrescription, self).__init__()
		self.initAction()
		self.init_layout()

	def initAction(self):
		self.searchBox = QtGui.QLineEdit()
		self.searchBox.setPlaceholderText('Search for a drug or salt')
		self.searchButton = QtGui.QPushButton('Search')
		self.connect(self.searchButton,QtCore.SIGNAL('clicked()'),self.queryProcess)

		self.searchBox.setStyleSheet("color:black;background-color:white")
		self.searchButton.setStyleSheet("color:black;background-color:lightgray")

		self.table = QtGui.QTableWidget()
		self.table.setStyleSheet("color:black;background-color:white")
		self.table.setColumnCount(5)
		columns = ['ItemID', 'Name', 'Type', 'Unit Price', 'Available Units']
		self.table.setHorizontalHeaderLabels(columns)
		self.table.setSortingEnabled(True)
		self.table.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.table.resizeColumnsToContents()

		self.addButton = QtGui.QPushButton('Add Selected item to Bill')
		self.addButton.setStyleSheet("color:black;background-color:lightgray")
		self.connect(self.addButton,QtCore.SIGNAL('clicked()'),self.addToBill)


		columnsForBill = ['ItemID', 'Name', 'Qty', 'Unit Price', 'Amount']
		self.bill = QtGui.QTableWidget()
		self.bill.setStyleSheet("color:black;background-color:white")
		self.bill.setColumnCount(5)
		self.bill.setRowCount(1)
		self.bill.setHorizontalHeaderLabels(columnsForBill)
		self.bill.setSortingEnabled(True)
		self.bill.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.bill.resizeColumnsToContents()

		self.removeButton = QtGui.QPushButton('Remove Selected from Bill')
		self.connect(self.removeButton,QtCore.SIGNAL('clicked()'),self.removeFromBill)
		self.removeButton.setStyleSheet("color:black;background-color:lightgray")

		self.modeCombo = QtGui.QComboBox()
		self.modeCombo.setStyleSheet("color:black;background-color:lightgray")
		self.modeCombo.addItems(['Cash','Card','Cheque','Other'])

		self.clearButton = QtGui.QPushButton('Clear Bill Table')
		self.connect(self.clearButton,QtCore.SIGNAL('clicked()'),self.clearBill)
		self.printButton = QtGui.QPushButton('Make Transaction')
		self.connect(self.printButton,QtCore.SIGNAL('clicked()'),self.printBill)

		self.clearButton.setStyleSheet("color:black;background-color:lightgray")
		self.printButton.setStyleSheet("color:black;background-color:lightgray")

	def init_layout(self):
		self.p_layout = QtGui.QGridLayout(self)
		self.p_layout.addWidget(self.searchBox,0,0,1,4)
		self.p_layout.addWidget(self.searchButton,0,4,1,1)
		self.p_layout.addWidget(self.table,1,0,4,-1)
		self.p_layout.addWidget(self.addButton,5,1,1,2)
		self.p_layout.addWidget(self.bill,6,0,4,-1)

		self.p_layout.addWidget(self.removeButton,10,1,1,2)
		self.p_layout.addWidget(self.clearButton,11,2,1,1)
		self.p_layout.addWidget(self.modeCombo,11,3,1,1)
		self.p_layout.addWidget(self.printButton,11,4,1,1)

	def queryProcess(self):
		self.query = str(self.searchBox.text())
		self.query = '%'+self.query+'%'
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()

			cur.execute('SELECT * FROM Stock WHERE NAME LIKE %s',(self.query))
			self.items = cur.fetchall()

			db.close()

			self.viewInTable()
		except Exception, e:
			print repr(e)

	def viewInTable(self):
		self.table.clear()
		columns = ['ItemID', 'Name', 'Type', 'Unit Price', 'Available Units']
		self.table.setHorizontalHeaderLabels(columns)
		numItem = len(self.items)
		self.table.setRowCount(numItem)

		for i in range(numItem):
			for j in range(5):
				it = QtGui.QTableWidgetItem()
				it.setText(str(self.items[i][j]))
				self.table.setItem(i,j,it)


	def printBill(self):
		if self.dataComplete():
			self.updateStock()
			mode = self.modeCombo.currentText()
			amount = float(str(self.bill.item(self.bill.rowCount()-1,4).text()))
			self.updateAccounts(mode=mode,amount=amount)
			msg = QtGui.QMessageBox()
			msg.setText('The transaction is successful.\nDatabase updated.')
			msg.setIcon(QtGui.QMessageBox.Information)
			msg.exec_()

	def addToBill(self):
		try:
			indexes = self.table.selectionModel()
			i = indexes.selectedRows()[0]
			pre = self.bill.rowCount()
			if pre is 0:
				self.bill.insertRow(pre)
				pre = self.bill.rowCount()
			self.bill.insertRow(pre)
			for j in range(2):
				it = QtGui.QTableWidgetItem(self.table.item(i.row(),j))
				self.bill.setItem(pre-1,j,it)

			it = QtGui.QTableWidgetItem(self.table.item(i.row(),3))
			self.bill.setItem(pre-1,3,it)
		except Exception, e:
			print repr(e)


	def removeFromBill(self):
		try:
			indexes = self.bill.selectionModel()
			i = indexes.selectedRows()[0]
			self.bill.removeRow(i.row())
		except Exception, e:
			print repr(e)


	def clearBill(self):
		self.bill.clear()
		columnsForBill = ['ItemID', 'Name', 'Qty', 'Unit Price', 'Amount']
		self.bill.setHorizontalHeaderLabels(columnsForBill)
		self.bill.setRowCount(1)

	def updateStock(self):
		total = self.bill.rowCount()
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()
			updateQuery = 'UPDATE Stock SET AvailableQty = AvailableQty-%s WHERE ItemID = %s'
			for i in range(total-2):
				try:
					cur.execute(updateQuery,(self.bill.item(i,2).text(),self.bill.item(i,0).text()))
				except Exception, e:
					print repr(e)
			db.commit()
			db.close()
		except Exception, e:
			db.rollback()
			db.close()
			print repr(e)

	def updateAccounts(self,mode='cash',date=datetime.datetime.now().strftime('%Y-%m-%d'),purpose='store',amount=0.0):
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()
			cur.execute('INSERT INTO Accounts (DOPayment,Mode,Purpose,Amount) VALUES (%s,%s,%s,%s)',(date,mode,purpose,amount))
			db.commit()
			db.close()
		except Exception, e:
			db.rollback()
			db.close()
			print repr(e)


	def dataComplete(self):
		try:
			total = self.bill.rowCount()
			amount = 0.0
			for i in range(total-1):
				if self.bill.item(i,2) is not None:
					temp = float(str(self.bill.item(i,2).text()))*float(str(self.bill.item(i,3).text()))
					amount += temp
					it  = QtGui.QTableWidgetItem()
					it.setText(str(temp))
					self.bill.setItem(i,4,it)
				else :
					raise Exception('data incomplete')

			# self.bill.insertRow(self.bill.rowCount())
			self.bill.insertRow(self.bill.rowCount())
			amountItem = QtGui.QTableWidgetItem()
			amountItem.setFlags(amountItem.flags() & ~QtCore.Qt.ItemIsEditable)
			amountItem.setText(str(amount))
			self.bill.setItem(self.bill.rowCount()-1,4,amountItem)
			totalItem = QtGui.QTableWidgetItem()
			totalItem.setFlags(totalItem.flags() & ~QtCore.Qt.ItemIsEditable)
			totalItem.setText('Total')
			self.bill.setItem(self.bill.rowCount()-1,3,totalItem)
			return True
		except:
			msg = QtGui.QMessageBox()
			msg.setText('Data Incomplete in bill!\nTry Again after filling bill.')
			msg.setIcon(QtGui.QMessageBox.Critical)
			msg.exec_()
			return False