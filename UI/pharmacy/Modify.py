__author__ = 'batman'

from PyQt4 import QtGui,QtCore
import MySQLdb as mdb

class Modify(QtGui.QWidget):
	def __init__(self):
		super(Modify, self).__init__()
		self.initAction()
		self.init_layout()

	def initAction(self):
		self.searchBox = QtGui.QLineEdit()
		self.searchBox.setPlaceholderText('Search for a drug by name or id')
		self.searchButton = QtGui.QPushButton('Search')
		self.connect(self.searchButton,QtCore.SIGNAL('clicked()'),self.queryProcess)

		self.searchBox.setStyleSheet("color:black;background-color:white")
		self.searchButton.setStyleSheet("color:black;background-color:lightgray")

		self.table = QtGui.QTableWidget()
		self.table.setStyleSheet("color:black;background-color:white")
		self.table.setColumnCount(5)
		columns = ['ItemID', 'Name', 'Type', 'Unit Price', 'Available Units']
		self.table.setHorizontalHeaderLabels(columns)
		self.table.hideColumn(5)
		self.table.setSortingEnabled(True)
		self.table.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.table.resizeColumnsToContents()
		self.addRows()

		self.addButton = QtGui.QPushButton('+')
		self.addButton.setStyleSheet("color:black;background-color:lightgray")
		self.connect(self.addButton,QtCore.SIGNAL('clicked()'),self.addRows)

		self.removeButton = QtGui.QPushButton('Delete Selected Row')
		self.removeButton.setStyleSheet("color:black;background-color:lightgray")
		self.connect(self.removeButton,QtCore.SIGNAL('clicked()'),self.deleteSelected)

		self.saveButton = QtGui.QPushButton('Update Changes')
		self.saveButton.setStyleSheet("color:black;background-color:lightgray")
		self.connect(self.saveButton,QtCore.SIGNAL('clicled()'),self.updateDatabase)

		self.clearButton = QtGui.QPushButton('Clear')
		self.clearButton.setStyleSheet("color:black;background-color:lightgray")
		self.connect(self.clearButton,QtCore.SIGNAL('clicked()'),self.clearTable)

	def init_layout(self):
		self.p_layout = QtGui.QGridLayout(self)
		self.p_layout.addWidget(self.searchBox,0,0,1,4)
		self.p_layout.addWidget(self.searchButton,0,4,1,1)
		self.p_layout.addWidget(self.table,1,0,4,-1)
		self.p_layout.addWidget(self.addButton,5,0,1,-1)
		self.p_layout.addWidget(self.removeButton,6,2,1,1)
		self.p_layout.addWidget(self.saveButton,6,3,1,1)
		self.p_layout.addWidget(self.clearButton,6,4,1,1)

	def queryProcess(self):
		self.query = str(self.searchBox.text())
		self.id = self.query
		self.query = '%'+self.query+'%'
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()

			try:
				cur.execute('SELECT * FROM Stock WHERE NAME LIKE %s',(self.query))
				self.items = cur.fetchall()
				if len(self.items)==0:
					cur.execute('SELECT * FROM Stock WHERE ItemID = %s',(self.id))
					self.items = cur.fetchall()
			except Exception, e:
				print repr(e)

			db.close()
			self.viewInTable()
		except Exception, e:
			msg = QtGui.QMessageBox()
			msg.setText('Error Occurred.\n'+repr(e))
			msg.setIcon(QtGui.QMessageBox.Critical)
			msg.exec_()
			print repr(e)


	def viewInTable(self):
		self.table.clear()
		columns = ['ItemID', 'Name', 'Type', 'Unit Price', 'Available Units']
		self.table.setHorizontalHeaderLabels(columns)
		numItem = len(self.items)
		self.table.setRowCount(numItem)

		for i in range(numItem):
			itId = QtGui.QTableWidgetItem()
			itId.setFlags(itId.flags() & ~QtCore.Qt.ItemIsEditable)
			itId.setText(str(self.items[i][0]))
			self.table.setItem(i,0,itId)
			for j in range(4):
				it = QtGui.QTableWidgetItem()
				it.setText(str(self.items[i][j+1]))
				self.table.setItem(i,j+1,it)

		self.addRows()


	def addRows(self):
		for i in range(20):
			self.table.insertRow(self.table.rowCount())
			itId = QtGui.QTableWidgetItem()
			itId.setFlags(itId.flags() & ~QtCore.Qt.ItemIsEditable)
			self.table.setItem(self.table.rowCount()-1,0,itId)

	def updateDatabase(self):
		total = self.table.rowCount()
		defaultItemText = self.table.item(total-1,0).text()
		try:
			db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
			cur = db.cursor()

			insertQuery = 'INSERT INTO Stock (NAME,TYPE,UNITPRICE,AVAILABLEQTY) VALUES (%s,%s,%s,%s)'
			updateQuery = 'UPDATE Stock SET NAME=%s, TYPE=%s, UNITPRICE=%s, AVAILABLEQTY=%s WHERE ITEMID=%s'
			for i in range(total):
				if self.table.item(i,0).text() != defaultItemText:
					try:
						ID = self.table.item(i,0).text()
						Name = self.table.item(i,1).text() if self.table.item(i,1) is not None else ''
						Type = self.table.item(i,2).text() if self.table.item(i,2) is not None else ''
						UNITPRICE = self.table.item(i,3).text() if self.table.item(i,3) is not None else ''
						AVAILABLEQTY = self.table.item(i,4).text() if self.table.item(i,4) is not None else ''
						cur.execute(updateQuery,(Name,Type,UNITPRICE,AVAILABLEQTY,ID))
					except Exception, e:
						print repr(e)
				elif self.table.item(i,1) is not None:
					try:
						Name = self.table.item(i,1).text()
						Type = self.table.item(i,2).text() if self.table.item(i,2) is not None else ''
						UNITPRICE = self.table.item(i,3).text() if self.table.item(i,3) is not None else ''
						AVAILABLEQTY = self.table.item(i,4).text() if self.table.item(i,4) is not None else ''
						cur.execute(insertQuery,(Name,Type,UNITPRICE,AVAILABLEQTY))
					except Exception, e:
						print repr(e)

			db.commit()
			db.close()
			self.clearTable()
			msg = QtGui.QMessageBox()
			msg.setText('Database updated.')
			msg.setIcon(QtGui.QMessageBox.Information)
			msg.exec_()
		except Exception, e:
			msg = QtGui.QMessageBox()
			msg.setText('Error Occurred.\n'+repr(e)+'\nDatabase updated.')
			msg.setIcon(QtGui.QMessageBox.Information)
			msg.exec_()
			db.rollback()
			db.close()
			print repr(e)

	def clearTable(self):
		self.table.clear()
		self.table.setRowCount(0)
		columns = ['ItemID', 'Name', 'Type', 'Unit Price', 'Available Units']
		self.table.setHorizontalHeaderLabels(columns)
		self.addRows()

	def deleteSelected(self):
		try:
			indexes = self.table.selectionModel()
			i = indexes.selectedRows()[0]
			defaultItemText = self.table.item(self.table.rowCount()-1,0).text()
			if self.table.item(i.row(),0).text() != defaultItemText:
				# Add a Question messagebox here.
				db = mdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
				cur = db.cursor()
				try:
					cur.execute('DELETE FROM Stock WHERE ItemID = %s',(self.table.item(i.row(),0).text()))
					db.commit()
				except Exception, e:
					db.rollback()
					print repr(e)

				db.close()

			self.table.removeRow(i.row())
			msg = QtGui.QMessageBox()
			msg.setText('Deletion successful.\nDatabase updated.')
			msg.setIcon(QtGui.QMessageBox.Information)
			msg.exec_()

		except Exception, e:
			msg = QtGui.QMessageBox()
			msg.setText('Error Occurred\n'+repr(e)+'\nDatabse updated.')
			msg.setIcon(QtGui.QMessageBox.Critical)
			msg.exec_()
			print repr(e)