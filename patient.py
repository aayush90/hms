import MySQLdb


class Patient(object):
	# PatID required to create each Patient object
	# not passed as parameter since each function requires it
	# create object when patient logs in

	def __init__(self,id):
		self.PatID=id

	def getPersonalInfo(self):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select PID,Password from Patient where PatID='%d' " %(self.PatID))
		tpl = cursor.fetchone()
		pid=tpl[0]
		password=tpl[1]
		cursor.execute("select * from Person where PID='%d' " %(pid))
		tpl=cursor.fetchone()
		db.close()
		dic={}
		if tpl[1]:
			dic['DOB']=str(tpl[1])
		else :
			dic['DOB']=''
		if tpl[2]:
			dic['Name']=tpl[2]
		else:
			dic['Name']=''
		if tpl[3]:
			dic['Gender']=tpl[3]
		else :
			dic['Gender']=''
		if tpl[4]:
			dic['Address']=tpl[4]
		else:
			dic['Address']=''
		if tpl[5]:
			dic['Phone']=tpl[5]
		else :
			dic['Phone']=''
		if password:
			dic['Password']=password
		else :
			dic['Password']=''
		return dic

	def viewAppointment(self): # returns list of dictionaries containing further appointments, time > now()
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select * from PatientEntry where PatID='%d' and AppointmentTime>now()" % (self.PatID))
		rl=cursor.fetchall()
		tos=[]
		for i in rl:
			dic={}
			if i[5]:
				dic['AppointmentTime']=str(i[5])
			else:
				dic['AppointmentTime']=''
			if i[3]:
				dic['IsHomeVisit']=i[3]
			else :
				dic['IsHomeVisit']=0
			cursor.execute("select * from Doctor where DID='%d' " %(i[1]))
			tpl=cursor.fetchone()
			pid=tpl[1]
			cursor.execute("select * from Person where PID='%d' " %(pid))
			dinfo=cursor.fetchone()
			dic['DoctorName']=dinfo[2]
			dic['Qualification']=tpl[5]
			dic['Department']=tpl[7]
			tos.append(dic)
		db.close()
		return tos

	def getOTschedule(self): # returns list of dictionaries containing further operations
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select distinct StartTime,FID,EndTime from OTschedule where PatID='%d' "%(self.PatID))
		ol=cursor.fetchall()
		tos=[]
		for x in ol:
			dic={}
			cursor.execute("select Name from Facility where FID = '%d' "%(x[1]))
			dic['Name']=cursor.fetchone()[0]
			dic['StartTime']=str(x[0])
			dic['EndTime']=str(x[2])
			dic['Doctors']=[]
			cursor.execute("select distinct DID from OTschedule where PatID='%d' and StartTime='%s' " %(self.PatID,str(x[0])) )
			dl=cursor.fetchall()
			for i in dl:
				dic1={}
				did=i[0]
				cursor.execute("select * from Doctor where DID='%d' " %(did))
				tpl=cursor.fetchone()
				pid=tpl[1]
				cursor.execute("select * from Person where PID='%d' " %(pid))
				dinfo=cursor.fetchone()
				dic1['DoctorName']=dinfo[2]
				dic1['Qualification']=tpl[5]
				dic1['Department']=tpl[7]
				dic['Doctors'].append(dic1)
			tos.append(dic)
		db.close()
		return tos

	def getOperationInfo(self):
		# returns list of dictionaries containing previous operations, each contains list of doctors operating
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select distinct FID,DOoper,Result from OperationInfo where PatID='%d'"%(self.PatID))
		ol=cursor.fetchall()
		tos=[]
		for x in ol:
			dic={}
			cursor.execute("select Name from Facility where FID = '%d' "%(x[0]))
			dic['Name']=cursor.fetchone()[0]
			dic['Date']=str(x[1])
			if x[2]:
				dic['Result']=x[2]
			else :
				dic['Result']=''
			dic['Doctors']=[]
			cursor.execute("select distinct DID from OperationInfo where PatID='%d' and DOoper='%s' and FID='%d' " %(self.PatID,str(x[1]),x[0]) )
			dl=cursor.fetchall()
			for i in dl:
				dic1={}
				did=i[0]
				cursor.execute("select * from Doctor where DID='%d' " %(did))
				tpl=cursor.fetchone()
				pid=tpl[1]
				cursor.execute("select * from Person where PID='%d' " %(pid))
				dinfo=cursor.fetchone()
				dic1['DoctorName']=dinfo[2]
				dic1['Qualification']=tpl[5]
				dic1['Department']=tpl[7]
				dic['Doctors'].append(dic1)
			tos.append(dic)
		db.close()
		return tos

	def getTestInfo(self):
		# returns list of dictionaries containing previous tests
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select distinct FID,DOtest,Report from TestInfo where PatID='%d'"%(self.PatID))
		ol=cursor.fetchall()
		tos=[]
		for x in ol:
			dic={}
			cursor.execute("select Name from Facility where FID = '%d' "%(x[0]))
			dic['Name']=cursor.fetchone()[0]
			dic['Date']=str(x[1])
			if x[2]:
				dic['Result']=x[2]
			else :
				dic['Result']=''
			tos.append(dic)
		db.close()
		return tos

	def getCheckUpInfo(self):
		# returns list of dictionaries containing previous check-ups
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select * from CheckUp where PatID='%d'"%(self.PatID))
		cl=cursor.fetchall()
		tos=[]
		for i in cl:
			dic={}
			did=i[2]
			cursor.execute("select * from Doctor where DID='%d' " %(did))
			tpl=cursor.fetchone()
			pid=tpl[1]
			cursor.execute("select * from Person where PID='%d' " %(pid))
			dinfo=cursor.fetchone()
			dic['DoctorName']=dinfo[2]
			dic['Qualification']=tpl[5]
			dic['Department']=tpl[7]
			dic['CheckUpTime']=str(i[3])
			if i[4]:
				dic['Diagnosis']=i[4]
			else:
				dic['Diagnosis']=''
			if i[5]:
				dic['Treatment']=i[5]
			else:
				dic['Treatment']=''
			if i[6]:
				dic['tobeAdmitted']=i[6]
			else:
				dic['tobeAdmitted']=0
			tos.append(dic)
		db.close()
		return tos




	def getAdmitInfo(self):
		# only for admitted patients
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select * from AdmitInfo where PatID='%d' "%(self.PatID))
		tpl=cursor.fetchone()
		dic={}
		if tpl:
			dic['Date']=str(tpl[2])
			dic['RoomNo']=tpl[3]
			dic['Building']=tpl[4]
			cursor.execute("select Name from Nurse natural join Person where NId='%d'"%(tpl[1]))
			dic['Nurse']=cursor.fetchone()[0]
			db.close()
		return dic

	def getTransactionDetails(self):
		# returns list of dictionaries containing previous transactions
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select * from Accounts where PatID='%d'"%(self.PatID))
		tl=cursor.fetchall()
		tos=[]
		for i in tl:
			dic={}
			dic['Purpose']=i[3]
			dic['Date']=str(i[1])
			dic['Mode']=i[2]
			dic['Amount']=i[4]
			dic['Medicines']=[]
			if i[3]=='store':
				cursor.execute("select Name,Quantity from TransactionDetails natural join Stock where TID='%d'"%(i[0]))
				ml=cursor.fetchall()
				for j in ml:
					dic1={}
					dic1['Name']=j[0]
					dic1['Quantity']=j[1]
					dic['Medicines'].append(dic1)
			tos.append(dic)
		db.close()
		return  tos


	def updateInfo(self,info):
		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		try:
			cursor.execute("select PID from Patient where PatID='%d'"%(self.PatID))
			row = cursor.fetchone()
			cursor.execute("update Patient set Password='%s' where PatID='%d'"%(info['Password'],self.PatID))
			cursor.execute("update Person set Name='%s',Address='%s',Phone='%s' where PID='%d'"%(info["name"],info["addr"],info["phone"],row[0]))
			db.commit()
			db.close()
			return 1
		except Exception as e:
			db.rollback()
			print(e)
			return 0



	def bookAppointment(self,isHomeVisit,DoctorID):
		pass
