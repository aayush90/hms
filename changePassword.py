from PyQt4.QtCore import *
from PyQt4.QtGui import *
import MySQLdb
import sys



class changePassword(QFrame):
	def __init__(self,text,id):
		super(changePassword,self).__init__()
		self.text = text
		self.id = id
		self.initUI()

	def initUI(self):


		self.oldpassword = QLabel("Old Password")
		self.oldpassword.setAlignment(Qt.AlignCenter)
		self.ioldpassword = QLineEdit()
		self.ioldpassword.setPlaceholderText("Enter your Old Password")
		self.newpassword = QLabel("New Password")
		self.newpassword.setAlignment(Qt.AlignCenter)
		self.inewpassword = QLineEdit()
		self.inewpassword.setPlaceholderText("Enter New Password")
		self.cnewpassword = QLabel("Confirm New Password")
		self.cnewpassword.setAlignment(Qt.AlignCenter)
		self.icnewpassword = QLineEdit()
		self.icnewpassword.setPlaceholderText("Enter New Password")

		# self.patient.setFixedWidth(150)
		self.ioldpassword.setFixedWidth(300)
		self.inewpassword.setFixedWidth(300)
		self.icnewpassword.setFixedWidth(300)

		self.ioldpassword.setEchoMode(QLineEdit.Password)
		self.inewpassword.setEchoMode(QLineEdit.Password)
		self.icnewpassword.setEchoMode(QLineEdit.Password)

		self.oldpassword.setStyleSheet("color: black; background-color:gray")
		self.newpassword.setStyleSheet("color: black; background-color:gray")
		self.cnewpassword.setStyleSheet("color: black; background-color:gray")

		self.inewpassword.setStyleSheet("color: black; background-color:white")
		self.ioldpassword.setStyleSheet("color: black; background-color:white")
		self.icnewpassword.setStyleSheet("color: black; background-color:white")

		self.grid = QGridLayout()
		self.grid.addWidget(self.oldpassword,0,0)
		self.grid.addWidget(self.ioldpassword,0,1)
		self.grid.addWidget(self.newpassword,1,0)
		self.grid.addWidget(self.inewpassword,1,1)
		self.grid.addWidget(self.cnewpassword,2,0)
		self.grid.addWidget(self.icnewpassword,2,1)

		self.hbox = QHBoxLayout()
		self.reset = QPushButton("Reset")
		self.enter = QPushButton("Enter")
		self.enter.setStyleSheet("color: black; background-color:gray")
		self.reset.setStyleSheet("color: black; background-color:gray")

		self.hbox.addStretch(1)
		self.hbox.addWidget(self.reset)
		self.hbox.addWidget(self.enter)
		self.hbox.addStretch(1)

		self.vbox = QVBoxLayout()
		self.vbox.addStretch(1)
		self.vbox.addLayout(self.grid)
		self.vbox.addLayout(self.hbox)
		self.vbox.addStretch(1)

		self.setLayout(self.vbox)
		self.setStyleSheet("background-color:lightblue;")


		self.connect(self.enter,SIGNAL("clicked()"),self.entry)
		self.connect(self.reset,SIGNAL("clicked()"),self.reset_all)


	def entry(self):
		self.pid = str(self.id)
		self.text = str(self.text)
		op = str(self.ioldpassword.text())
		np = str(self.inewpassword.text())
		cnp = str(self.icnewpassword.text())

		if len(op) == 0 or len(np) == 0 or len(cnp) == 0:
			message = QMessageBox(QMessageBox.Warning,"Error Message","All fields not entered. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return

		db = MySQLdb.connect(host='10.5.18.66',user='12CS10055',db='12CS10055',passwd='btech12')
		cursor=db.cursor()
		cursor.execute("select Password from %s where PID = %s"%(self.text,self.pid))
		result = cursor.fetchone()
		if op != result[0]:
			message = QMessageBox(QMessageBox.Warning,"Error Message","Entered old password is incorrect. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		if np!=cnp:
			message = QMessageBox(QMessageBox.Warning,"Error Message","Entered new password doesnot match. Try Again",buttons = QMessageBox.Close)
			message.exec_()
			return
		try:
			cursor.execute("UPDATE %s SET Password = '%s' WHERE PID = %s"%(self.text,np,self.pid))
			db.commit()
			message = QMessageBox(QMessageBox.Information,"Success Message","Password changed successfully.",buttons = QMessageBox.Close)
			message.exec_()
			self.reset_all()
		except Exception as e:
			print (e)
			db.rollback()
			message = QMessageBox(QMessageBox.Warning,"Error Message","Error occured. Try Again",buttons = QMessageBox.Close)
			message.exec_()
		db.close()

	def reset_all(self):
		self.inewpassword.clear()
		self.icnewpassword.clear()
		self.ioldpassword.clear()





# app = QApplication(sys.argv)
# start = changePassword("Doctor",315)
# start.show()
# app.exec_()
