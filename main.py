import sys
from UI.login import *
from UI.doctor.doctor import *
from UI.reception.receptionist import *
from UI.admin.admin import *
from UI.PatientGUI import *
from UI.pharmacy.Pharmacy import *

class HMS(QWidget):

	def __init__(self):
		super(HMS,self).__init__()
		self.doctor=Doctor(self)
		self.reception = Reception(self)
		self.admin = Admin(self)
		self.patient = PatientGUI(self)
		self.pharmacy = Pharmacy(self)
		self.initUI()
		self.showMaximized()

	def initUI(self):
		self.setWindowTitle('HMS')				 	# Window Title
		self.setWindowIcon(QIcon('sample3.jpg')) 	# App Icon
		self.docbtn = QPushButton("Doctor Login")
		self.adminbtn = QPushButton("Admin Login")
		self.recbtn = QPushButton("Receptionist Login")
		self.patientbtn = QPushButton("Show Patient Details")
		self.pharmacybtn = QPushButton("Pharmacy")
		pic = QLabel()
		pixmap = QPixmap("sample4.jpg")
		#pixmap.scaled(pic.height(),pic.width())
		pixmap.scaled (pic.width(),pic.height(),aspectRatioMode = Qt.IgnoreAspectRatio, transformMode = Qt.FastTransformation)
		pic.setPixmap(pixmap)
		#pic.setMaximumSize(QSize(1700,1500))
		grid = QGridLayout()
		grid.addWidget(self.adminbtn,2,0,1,1)
		grid.addWidget(self.docbtn,3,0,1,1)
		grid.addWidget(self.recbtn,4,0,1,1)
		grid.addWidget(self.patientbtn,5,0,1,1)
		grid.addWidget(self.pharmacybtn,6,0,1,1)
		grid.addWidget(pic,0,4,9,7)
		pic.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
		pic.setStyleSheet("background-color:red")
		self.adminbtn.setStyleSheet("color: black; background-color:gray")
		self.docbtn.setStyleSheet("color: black; background-color:gray")
		self.recbtn.setStyleSheet("color: black; background-color:gray")
		self.patientbtn.setStyleSheet("color: black; background-color:gray")
		self.pharmacybtn.setStyleSheet("color: black; background-color:gray")
		self.setStyleSheet("background-color:white")
		self.setLayout(grid)

		self.connect(self.adminbtn,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.recbtn,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.docbtn,SIGNAL("clicked()"),self.buttonEvent)
		self.connect(self.patientbtn,SIGNAL("clicked()"),self.showPatient)
		self.connect(self.pharmacybtn,SIGNAL("clicked()"),self.showPharmacy)


	def updateUI(self,user,pid):
		self.close()
		if(user=="doctor"):
			self.doctor.showIT(pid)
		elif(user=="admin"):
			self.admin.showIT(pid)
		else:
			self.reception.showIT(pid)

	def buttonEvent(self):
		if(self.sender()==self.adminbtn):
			self.login = LogIn(self,"admin")
		elif(self.sender()==self.recbtn):
			self.login = LogIn(self,"reception")
		elif(self.sender()==self.docbtn):
			self.login = LogIn(self,"doctor")

		self.login.exec_()


	def showPatient(self):
		patid,ok=QInputDialog.getInt (self,QString("Patient ID"),QString("Enter Patient ID"),min=1)
		if ok:
			self.close()
			self.patient.showIT(patid)


	def showPharmacy(self):
		self.close()
		self.pharmacy.showMaximized()


# if __name__ == '__main__':
# 	app = QApplication(sys.argv)
# 	start = Pharmacy()
# 	start.show()
# 	# app.exec_()
# 	sys.exit(app.exec_())
app = QApplication(sys.argv)
app.setStyle(QtGui.QStyleFactory.create('cleanlooks'))
start = HMS()
app.exec_()
